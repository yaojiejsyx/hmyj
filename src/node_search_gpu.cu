#include <iostream>
#include <omp.h>
#include <stdio.h>
#include <vector>

#include <cuda_runtime.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/scan.h>
#include <thrust/sort.h>

#include "../common/frame_structure.h"
#include "../common/mesh_type.h"
#include "../common/gpu_helper.h"
#include "../common/global_var_list.h"

using namespace std;

#define  G_FACTOR   0.5
#define  BREAK_DEEP 1

//extern Ref_Frame lab;
extern RectBox sq_box;

void initSearch(void)
{
//    GpuTimer timer_01; timer_01.Start();
    
    cudaMemcpy(GType2_dev, GType2, Point_All*sizeof(GType::type), cudaMemcpyHostToDevice);
    cudaMemcpy(GType1_dev, GType1, Point_All*sizeof(GType::type), cudaMemcpyHostToDevice);
//    cudaMemcpy(GTypeT_dev, GTypeT, Point_All*sizeof(GType::type), cudaMemcpyHostToDevice);
    
    cudaMemcpy(MNx_dev, MNx, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(MNy_dev, MNy, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(MNz_dev, MNz, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    
//    timer_01.Stop(); cout<<"\tTime Used for search init-1: "<<timer_01.Elapsed()<<" ms"<<endl;
    
    cudaMemcpy(X_cld_dev, X_Meshless, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(Y_cld_dev, Y_Meshless, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(Z_cld_dev, Z_Meshless, Point_Meshless*sizeof(double), cudaMemcpyHostToDevice);
    
//    cudaMemcpy(IIndex_dev, IIndex, IPoint*sizeof(int), cudaMemcpyHostToDevice);
//    cudaMemcpy(JIndex_dev, JIndex, JPoint*sizeof(int), cudaMemcpyHostToDevice);
//    cudaMemcpy(ONI_dev, ONI, Total_Outer_Nodes*sizeof(int), cudaMemcpyHostToDevice);
    
//    cudaMemcpy(Cordx_dev, Cord[0], IPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
//    cudaMemcpy(Cordy_dev, Cord[1], JPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
//    cudaMemcpy(Cordz_dev, Cord[2], KPoint*sizeof(CoordMember), cudaMemcpyHostToDevice);
    
//    timer_01.Stop(); cout<<"\tTime Used for search init-2: "<<timer_01.Elapsed()<<" ms"<<endl;
    
    cudaDeviceSynchronize(); getLastCudaError("Initialize_Search");
}

//block.x = 256; block.y = 1; block.z = 1;
//grid.x  = iDivUp(Point_Car, block.x); grid.y = 1; grid.z = 1;
__global__
void generateList_kernel(uint* list, const uint* mark, const uint n)
{
    uint ijk = blockIdx.x * blockDim.x + threadIdx.x;
    
    if (ijk>=n || ijk == 0) return;
    
    if (mark[ijk] != mark[ijk-1])
    {
        list[mark[ijk-1]] = ijk;
    }
}

//block.x = 256; block.y = 1; block.z = 1;
//grid.x  = iDivUp(Point_Car, block.x); grid.y = 1; grid.z = 1;
__global__
void setGTypeT_kernel(GType::type* GTypeT, GType::type* GType2, const int Point_Car, int FSI_Iter)
{
    uint ijk = blockIdx.x * blockDim.x + threadIdx.x;
    if (ijk >= Point_Car) return; 
    
    GTypeT[ijk] = GType2[ijk];
    if (FSI_Iter == 1)
	{
		if (GType2[ijk]==4) GTypeT[ijk] = 3;
	}
    // reset all cartesian nodes to GType::_7POINT, except boundaries
    if (GType2[ijk]!=2 && GType2[ijk]!=6) GType2[ijk] = 1;
}

//block.x = 256; block.y = 1; block.z = 1;
//grid.x  = iDivUp(Point_Car, block.x); grid.y = 1; grid.z = 1;
__global__
void searchOldT4_kernel(GType::type* GType2, uint* cand_mark, const GType::type *GType1, const GType::type *GTypeT, const int Point_Car)
{
    uint ijk = blockIdx.x * blockDim.x + threadIdx.x;
    if (ijk >= Point_Car) return; 
    
    if ((GType1[ijk] == GType::INACTIVE || GTypeT[ijk] == GType::INACTIVE || (GType1[ijk] == 3 && GTypeT[ijk] == 4)))
    {
        GType2[ijk] = GType::CAND_4;
        cand_mark[ijk] = 1;
    }
}

//block.x = idx_end[2]-idx_start[2]+1; block.y = 1; block.z = 1;
//grid.x  = idx_end[0]-idx_start[0]+1; grid.y  = idx_end[1]-idx_start[1]+1; grid.z  = 1;
__global__ 
void searchCandidates_kernel
(
    GType::type* GType2, uint* cand_mark, const GType::type* GType1, 
    const double* X_cld, const double* Y_cld, const double* Z_cld,
    const double* MNx, const double* MNy, const double* MNz,
    CoordMember* const Cordx, CoordMember* const Cordy, CoordMember* const Cordz,
    const int* IIndex, const int* JIndex, int Point_Car,
    const int* outer_idx, const uint* obj_info, int n_obj,
    const int Istart, const int Jstart, const int Kstart, 
    const int Iend,   const int Jend,   const int Kend
)
{
    int i = blockIdx.x + Istart;
    int j = blockIdx.y + Jstart;
    int k = threadIdx.x + Kstart;
    
    if (i>Iend || j>Jend || k>Kend) return;
    
    int ijk = IIndex[i] + JIndex[j] + k;
    
    if (GType2[ijk] == GType::MUTE || GType2[ijk] == GType::OUTFLOW) 
    {
//        n_err++;
#       if __CUDA_ARCH__ >= 200
        printf("*** Cloud is too close to the boundary! ***\n");
#       endif
        return;
    }
    
    uint id_nearest;
    double dist, dist_min;
    double xyz[3] = { Cordx[i].pos, Cordy[j].pos, Cordz[k].pos };
    
    for (int id=0; id<n_obj; id++)
    {
        dist = dist_min = 1.0e6;
        
        uint outer_ofs = obj_info[id];
        uint outer_num = obj_info[id + n_obj];
        
        for (int im=0; im<outer_num; im++)
        {
            uint iter = outer_idx[im + outer_ofs] - Point_Car;
            dist = (xyz[0] - X_cld[iter]) * (xyz[0] - X_cld[iter])
                + (xyz[1] - Y_cld[iter]) * (xyz[1] - Y_cld[iter])
                + (xyz[2] - Z_cld[iter]) * (xyz[2] - Z_cld[iter]);
            
            if (dist < dist_min)
            {
                dist_min = dist;
                id_nearest = iter;
            }
        }
        
        dist = MNx[id_nearest]*(xyz[0] - X_cld[id_nearest])
            + MNy[id_nearest]*(xyz[1] - Y_cld[id_nearest])
            + MNz[id_nearest]*(xyz[2] - Z_cld[id_nearest]);
        
        if (dist < 0)
        {
            if (GType2[ijk] == GType::CAND_4)
                GType2[ijk] = GType::OVLP_4;
            else
                GType2[ijk] = GType::OVLP_3;
            
            cand_mark[ijk] = 1;
            break;
        }
    }
}

// block.x = 256; block.y = 1; block.z = 1;
// grid.x  = iDivUp(n_cand, block.x); grid.y = 1; grid.z = 1;
__global__
void break_kernel
(
    GType::type* GType2, const uint* cand_list, const int size,
    const double* X_cld, const double* Y_cld, const double* Z_cld,
    const double* MNx, const double* MNy, const double* MNz,
    CoordMember* const Cordx, CoordMember* const Cordy, CoordMember* const Cordz,
    const int* IIndex, const int* JIndex, const int Point_Car, const double delta,
    const int* inner_idx, uint* obj_info, int n_obj,
    const int IPoint, const int JPoint, const int KPoint
)
{
    uint it = blockIdx.x * blockDim.x + threadIdx.x;
    if (it>=size) return;
    uint ijk = cand_list[it];
    
    if (GType2[ijk]!=GType::OVLP_3 && GType2[ijk]!=GType::OVLP_4) return;
    
    int ij = int(ijk/KPoint);
    int k = ijk%KPoint;
    int i = ij/JPoint;
    int j = ij%JPoint;
    
//    if ((IIndex[i] + JIndex[j] + k) != ijk)
//    {
//#       if __CUDA_ARCH__ >= 200
//        printf("*** GTYPE error! point's type were not converted. ***\n");
//#       endif
//    }
    
    uint ijk_w = IIndex[i-1] + JIndex[j] + k;
    uint ijk_e = IIndex[i+1] + JIndex[j] + k;
    uint ijk_s = IIndex[i] + JIndex[j-1] + k;
    uint ijk_n = IIndex[i] + JIndex[j+1] + k;
    
    if (GType2[ijk-1]==GType::_7POINT || GType2[ijk+1]==GType::_7POINT
        || GType2[ijk_w]==GType::_7POINT || GType2[ijk_e]==GType::_7POINT
        || GType2[ijk_s]==GType::_7POINT || GType2[ijk_n]==GType::_7POINT
        || GType2[ijk-1]==GType::MOBILIZED || GType2[ijk+1]==GType::MOBILIZED
        || GType2[ijk_w]==GType::MOBILIZED || GType2[ijk_e]==GType::MOBILIZED
        || GType2[ijk_s]==GType::MOBILIZED || GType2[ijk_n]==GType::MOBILIZED)
    {
        if (GType2[ijk] == GType::OVLP_4)
            GType2[ijk] = GType::CAND_4;
        else if (GType2[ijk] == GType::OVLP_3)
            GType2[ijk] = GType::CAND_3;
    }
    else
    {
        return;
    }
    
    uint id_nearest;
    double dist, dist_min;
    double xyz[3] = { Cordx[i].pos, Cordy[j].pos, Cordz[k].pos };
    
    for (int id=0; id<n_obj; id++)
    {
        uint inner_ofs = obj_info[id + 2*n_obj];
        uint inner_num = obj_info[id + 3*n_obj];
        
        dist = dist_min = 1.0e6;
        for (int im=0; im<inner_num; im++)
        {
            uint iter = inner_idx[im + inner_ofs] - Point_Car;
            dist = (xyz[0] - X_cld[iter]) * (xyz[0] - X_cld[iter])
                + (xyz[1] - Y_cld[iter]) * (xyz[1] - Y_cld[iter])
                + (xyz[2] - Z_cld[iter]) * (xyz[2] - Z_cld[iter]);
            
            if (dist < dist_min)
            {
                dist_min = dist;
                id_nearest = iter;
            }
        }
        
        dist = MNx[id_nearest]*(xyz[0] - X_cld[id_nearest])
            + MNy[id_nearest]*(xyz[1] - Y_cld[id_nearest])
            + MNz[id_nearest]*(xyz[2] - Z_cld[id_nearest]);
        
        if (dist < G_FACTOR*delta)
        {
            GType2[ijk] = GType::INACTIVE;
            obj_info[0] = 1;
            return;
        }
    }
}

// block.x = 256; block.y = 1; block.z = 1;
// grid.x  = iDivUp(n_cand, block.x); grid.y = 1; grid.z = 1;
__global__
void refresh_kernel(GType::type* GType2, const uint* cand_list, const int size)
{
    uint it = blockIdx.x * blockDim.x + threadIdx.x;
    if (it>=size) return;
    uint ijk = cand_list[it];
    
    if (GType2[ijk] == GType::CAND_3)
        GType2[ijk] = GType::MOBILIZED;
}

// block.x = 256; block.y = 1; block.z = 1;
// grid.x  = iDivUp(n_cand, block.x); grid.y = 1; grid.z = 1;
__global__
void refreshCandidates_kernel(GType::type* GType2, uint* mob_mark, uint* cnt, 
            const GType::type* GType1, const uint* cand_list, const uint size, const bool is_new)
{
    uint i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i>=size) return;
    uint ijk = cand_list[i];
    
    __shared__ uint sh_nfast, sh_nnewbie;
//    __shared__ uint sh_ninactive;
//    sh_ninactive = 0;
    sh_nfast = sh_nnewbie = 0;
    __syncthreads();
    
    if (GType2[ijk] == GType::OVLP_3 || GType2[ijk] == GType::OVLP_4 || GType2[ijk] == GType::INACTIVE)
    {
        GType2[ijk] = GType::INACTIVE;
//        atomicAdd(&sh_ninactive, 1);
        if (GType1[ijk] == GType::_7POINT && is_new)
        {
            atomicAdd(&sh_nfast, 1);
        }
    }
    else if (GType2[ijk] == GType::CAND_4)
    {
        GType2[ijk] = GType::NEWBIE;
        mob_mark[ijk] = 1;
        atomicAdd(&sh_nnewbie, 1);
    }
    else if (GType2[ijk] == GType::MOBILIZED)
    {
        mob_mark[ijk] = 1;
    }
    else
    {
#       if __CUDA_ARCH__ >= 200
        printf("*** GTYPE error! point's type were not converted. ***\n");
#       endif
        GType2[ijk] = GType::INACTIVE;
//        atomicAdd(&sh_ninactive, 1);
    }
    __syncthreads();
    
    if (threadIdx.x == 0) 
    {
//        atomicAdd(&cnt[2], sh_ninactive);
        atomicAdd(&cnt[0], sh_nfast);
        atomicAdd(&cnt[1], sh_nnewbie);
    }
}

//block.x = idx_end[2]-idx_start[2]+1; block.y = 1; block.z = 1;
//grid.x  = idx_end[0]-idx_start[0]+1; grid.y  = idx_end[1]-idx_start[1]+1; grid.z = 1;
__global__
void searchMobilizedNodes_kernel
(
    GType::type* GType2, uint* mob_mark,
    const int *IIndex, const int *JIndex, 
    const int Istart, const int Jstart, const int Kstart, 
    const int Iend,   const int Jend,   const int Kend
)
{
    uint i = blockIdx.x + Istart;
    uint j = blockIdx.y + Jstart;
    uint k = threadIdx.x + Kstart;
    
    if (i>Iend || j>Jend || k>Kend) return;
    
    uint ijk = IIndex[i] + JIndex[j] + k;
    
    if (GType2[ijk] == GType::_7POINT)
    {
        uint ijk_w = IIndex[i-1] + JIndex[j] + k;
        uint ijk_e = IIndex[i+1] + JIndex[j] + k;
        uint ijk_s = IIndex[i] + JIndex[j-1] + k;
        uint ijk_n = IIndex[i] + JIndex[j+1] + k;
        
#ifdef  UPWIND
        uint ijk_ww = IIndex[i-2] + JIndex[j] + k;
        uint ijk_ee = IIndex[i+2] + JIndex[j] + k;
        uint ijk_ss = IIndex[i] + JIndex[j-2] + k;
        uint ijk_nn = IIndex[i] + JIndex[j+2] + k;
        
        if (GType2[ijk-1] == GType::INACTIVE || GType2[ijk-1] == GType::NEWBIE 
            || GType2[ijk+1] == GType::INACTIVE || GType2[ijk+1] == GType::NEWBIE 
            || GType2[ijk_e] == GType::INACTIVE || GType2[ijk_e] == GType::NEWBIE 
            || GType2[ijk_w] == GType::INACTIVE || GType2[ijk_w] == GType::NEWBIE 
            || GType2[ijk_s] == GType::INACTIVE || GType2[ijk_s] == GType::NEWBIE 
            || GType2[ijk_n] == GType::INACTIVE || GType2[ijk_n] == GType::NEWBIE
            || GType2[ijk-2] == GType::INACTIVE || GType2[ijk-2] == GType::NEWBIE 
            || GType2[ijk+2] == GType::INACTIVE || GType2[ijk+2] == GType::NEWBIE 
            || GType2[ijk_ee] == GType::INACTIVE || GType2[ijk_ee] == GType::NEWBIE 
            || GType2[ijk_ww] == GType::INACTIVE || GType2[ijk_ww] == GType::NEWBIE 
            || GType2[ijk_ss] == GType::INACTIVE || GType2[ijk_ss] == GType::NEWBIE 
            || GType2[ijk_nn] == GType::INACTIVE || GType2[ijk_nn] == GType::NEWBIE) 
#else
        if (GType2[ijk-1] == GType::INACTIVE || GType2[ijk-1] == GType::NEWBIE 
            || GType2[ijk+1] == GType::INACTIVE || GType2[ijk+1] == GType::NEWBIE 
            || GType2[ijk_e] == GType::INACTIVE || GType2[ijk_e] == GType::NEWBIE 
            || GType2[ijk_w] == GType::INACTIVE || GType2[ijk_w] == GType::NEWBIE 
            || GType2[ijk_s] == GType::INACTIVE || GType2[ijk_s] == GType::NEWBIE 
            || GType2[ijk_n] == GType::INACTIVE || GType2[ijk_n] == GType::NEWBIE) 
#endif /*UPWIND*/
//            if (GType2[ijk-1] == GType::INACTIVE || GType2[ijk+1] == GType::INACTIVE 
//                || GType2[ijk_e] == GType::INACTIVE || GType2[ijk_w] == GType::INACTIVE 
//                || GType2[ijk_s] == GType::INACTIVE || GType2[ijk_n] == GType::INACTIVE
//                || GType2[ijk-1] == GType::NEWBIE || GType2[ijk+1] == GType::NEWBIE 
//                || GType2[ijk_e] == GType::NEWBIE || GType2[ijk_w] == GType::NEWBIE 
//                || GType2[ijk_s] == GType::NEWBIE || GType2[ijk_n] == GType::NEWBIE) 
        {
            GType2[ijk] = GType::MOBILIZED;
            mob_mark[ijk] = 1;
        }
    }

}

void updateGridType_GPU(const Ref_Frame& lab, const int *outer_offset, const int* inner_offset, const unsigned start[3], const unsigned end[3], int FSI_Iter, const bool is_new /*= true*/)
{
    initSearch();
    
//    GpuTimer timer_01; timer_01.Start();
    
	uint n_cand, n_mobilized, n_fast, n_newbie, n_obj = lab.obj_number; //, n_inactive
	int dim[3] = {IPoint, JPoint, KPoint}, idx_start[3], idx_end[3];
	dim3 block, grid;
	
	uint *mark_dev = NULL, *cand_list_dev = NULL, *obj_info_dev = NULL;
	size_t info_size;
	
	cudaMalloc((void**)&mark_dev, Point_Car*sizeof(uint));
    cudaMemset(mark_dev, 0, Point_Car*sizeof(uint));
    thrust::device_ptr<uint> mark_dp(mark_dev);
    
    info_size =  (n_obj * 4 > 32) ? n_obj*4 : 32;
    cudaMalloc((void**)&obj_info_dev, sizeof(uint) * info_size);
    thrust::device_ptr<uint> obj_info_dp(obj_info_dev);
    for (int id=0; id<n_obj; id++)
    {
        obj_info_dp[id] = outer_offset[id];
        obj_info_dp[id + n_obj] = lab.rigid_body[id]->OUTER_POINT_NUMBER;
        obj_info_dp[id + 2*n_obj] = inner_offset[id];
        obj_info_dp[id + 3*n_obj] = lab.rigid_body[id]->INNER_POINT_NUMBER;
    }
    
    // mark previous inactive nodes
    if (is_new)
    {
        block.x = 256; block.y = 1; block.z = 1;
        grid.x  = iDivUp(Point_Car, block.x); grid.y = 1; grid.z = 1;
        setGTypeT_kernel <<< grid, block >>> (GTypeT_dev, GType2_dev, Point_Car, FSI_Iter);
        
        block.x = 256; block.y = 1; block.z = 1;
        grid.x  = iDivUp(Point_Car, block.x); grid.y = 1; grid.z = 1;
        searchOldT4_kernel <<< grid, block >>> (GType2_dev, mark_dev, GType1_dev, GTypeT_dev, Point_Car);
        cudaDeviceSynchronize(); getLastCudaError("searchOldT4");
    }
    
    // mark overlapped nodes
	for (int i=0; i<3; i++)
	{
	    idx_start[i] = start[i] - 1;
	    if (idx_start[i] < 0) idx_start[i]=0;
	    
	    idx_end[i] = end[i] + 1;
	    if (idx_end[i] >= dim[i]) idx_end[i] = dim[i] - 1;
	}
	
    block.x = idx_end[2]-idx_start[2]+1; block.y = 1; block.z = 1;
    grid.x  = idx_end[0]-idx_start[0]+1; grid.y  = idx_end[1]-idx_start[1]+1; grid.z = 1;
    searchCandidates_kernel <<< grid, block >>> 
    (
        GType2_dev, mark_dev, GType1_dev, 
        X_cld_dev, Y_cld_dev, Z_cld_dev,
        MNx_dev, MNy_dev, MNz_dev,
        Cordx_dev, Cordy_dev, Cordz_dev,
        IIndex_dev, JIndex_dev, Point_Car,
        ONI_dev, obj_info_dev, n_obj,
        idx_start[0], idx_start[1], idx_start[2], 
        idx_end[0],   idx_end[1],   idx_end[2]
    );
    cudaDeviceSynchronize(); getLastCudaError("searchCanditate");
    
    // creat a list that stores the indices of candidates
    thrust::inclusive_scan(mark_dp, mark_dp + Point_Car, mark_dp);
    n_cand = mark_dp[Point_Car-1];
    
    if (cand_list_dev != NULL) { cudaFree(cand_list_dev); cand_list_dev = NULL; }
    cudaMalloc((void**)&cand_list_dev, n_cand*sizeof(uint));
    cudaMemset(cand_list_dev, 0, n_cand*sizeof(uint));
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(Point_Car, block.x); grid.y = 1; grid.z = 1;
    generateList_kernel <<< grid, block >>> (cand_list_dev, mark_dev, Point_Car);
    cudaDeviceSynchronize(); getLastCudaError("generateList");
    
    // break into meshless cloud
    bool end_search = false;
    int break_iter = 0;
    obj_info_dp[0] = 0;
    
    while (!end_search && break_iter<BREAK_DEEP)
    {
        block.x = 256; block.y = 1; block.z = 1;
        grid.x  = iDivUp(n_cand, block.x); grid.y = 1; grid.z = 1;
        break_kernel <<< grid, block >>>
        (
            GType2_dev, cand_list_dev, n_cand,
            X_cld_dev, Y_cld_dev, Z_cld_dev,
            MNx_dev, MNy_dev, MNz_dev,
            Cordx_dev, Cordy_dev, Cordz_dev,
            IIndex_dev, JIndex_dev, Point_Car, sq_box.delta,
            BNI_dev, obj_info_dev, n_obj,
            IPoint, JPoint, KPoint
        );
        cudaDeviceSynchronize(); getLastCudaError("break");
        if (obj_info_dp[0] != 0) end_search = true;
        
        block.x = 256; block.y = 1; block.z = 1;
        grid.x  = iDivUp(n_cand, block.x); grid.y = 1; grid.z = 1;
        refresh_kernel <<< grid, block >>> (GType2_dev, cand_list_dev, n_cand);
        cudaDeviceSynchronize(); getLastCudaError("refresh");
        break_iter++;
    }
    
    // refresh the type of candidate notes, mark mobilized nodes inside candidates
    cudaMemset(mark_dev, 0, Point_Car*sizeof(uint));
    cudaMemset(obj_info_dev, 0, sizeof(uint) * info_size);
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(n_cand, block.x); grid.y = 1; grid.z = 1;
    refreshCandidates_kernel <<< grid, block >>>
    (
        GType2_dev, mark_dev, obj_info_dev, GType1_dev, cand_list_dev, n_cand, is_new
    );
    
    n_fast = obj_info_dp[0];
    n_newbie = obj_info_dp[1];
//    n_inactive = obj_info_dp[2];
    if (n_fast > 0)
    {
        cout << "*** Cloud moves too fast! " << n_fast << " _7POINT points overlaped. " 
             << n_newbie << " NEWBIE points involved. ***" << endl;
    }
    
    // mark mobilized nodes outside the rigid body
    for (int i=0; i<3; i++)
	{
	    idx_start[i] = start[i] - 2;
	    if (idx_start[i] < 0) idx_start[i]=0;
	    
	    idx_end[i] = end[i] + 2;
	    if (idx_end[i] >= dim[i]) idx_end[i] = dim[i] - 1;
	}
	
    block.x = idx_end[2]-idx_start[2]+1; block.y = 1; block.z = 1;
    grid.x  = idx_end[0]-idx_start[0]+1; grid.y  = idx_end[1]-idx_start[1]+1; grid.z = 1;
    searchMobilizedNodes_kernel  <<< grid, block >>> 
    (
        GType2_dev, mark_dev, 
        IIndex_dev, JIndex_dev, 
        idx_start[0], idx_start[1], idx_start[2], 
        idx_end[0],   idx_end[1],   idx_end[2]
    );
    cudaDeviceSynchronize(); getLastCudaError("searchMobilizedNodes");
    
    // creat a list that stores the indices of mobilized nodes
    thrust::inclusive_scan(mark_dp, mark_dp + Point_Car, mark_dp);
    n_mobilized = mark_dp[Point_Car-1];
    
    if (cand_list_dev != NULL) { cudaFree(cand_list_dev); cand_list_dev = NULL; }
    cudaMalloc((void**)&cand_list_dev, n_mobilized*sizeof(uint));
    cudaMemset(cand_list_dev, 0, n_mobilized*sizeof(uint));
    cudaDeviceSynchronize(); getLastCudaError("generateList_malloc");
    
    block.x = 256; block.y = 1; block.z = 1;
    grid.x  = iDivUp(Point_Car, block.x); grid.y = 1; grid.z = 1;
    generateList_kernel <<< grid, block >>> (cand_list_dev, mark_dev, Point_Car);
    cudaDeviceSynchronize(); getLastCudaError("generateList_mob");
    
    // copy GType2 back onto host memory
    cudaMemcpy(GType2, GType2_dev, Point_All*sizeof(GType::type), cudaMemcpyDeviceToHost);
    cudaMemcpy(GTypeT, GTypeT_dev, Point_All*sizeof(GType::type), cudaMemcpyDeviceToHost);
    
    // copy the c2m list onto host vector C2M_List
    uint *c2m_list = new uint [n_mobilized];
    cudaMemcpy(c2m_list, cand_list_dev, n_mobilized*sizeof(uint), cudaMemcpyDeviceToHost);
    
    C2M_List.resize(n_mobilized);
#   pragma omp parallel for 
    for(int i=0; i<n_mobilized; i++) 
    {
        C2M_List[i].Meshless_Ind = c2m_list[i];
    }
    delete []c2m_list; c2m_list = NULL;
    
    cudaFree(mark_dev);
    cudaFree(cand_list_dev);
    cudaFree(obj_info_dev);
    
    cudaDeviceSynchronize(); getLastCudaError("updateGType_GPU");
//    timer_01.Stop(); cout<<"\tTime Used for Searching is: "<<timer_01.Elapsed()<<" ms"<<endl;
//    cout << "\tInactive cartesian nodes: " << n_inactive << ", mobilized cartesian nodes: " << n_mobilized << endl;
}


//int Compare_C2M_List(size_t list_size)
//{
//    int err = 0;
//    
//    err = compareArray(GType2_tmp, GType2, Point_All);
//    if (err) return err;
//    cout<<"   GType2 Checked"<<endl;
//    
//    int cpu_size = C2M_List.size();
//    if (cpu_size != list_size) {
//        cout<<"ERROR: List size not match!"<<endl;
//        err = 1;
//        return err;
//    }
//    cout<<"   Size Checked"<<endl;
//    
//    int *cpu_idx = new int [list_size];
//    int *gpu_idx = new int [list_size];
//    
//    for(int i=0; i<list_size; i++) {
//        cpu_idx[i] = C2M_List[i].Meshless_Ind;
//        gpu_idx[i] = C2M_List_tmp[i].Meshless_Ind;
//    }
//    thrust::sort(cpu_idx, cpu_idx + list_size);
//    cout<<"   CPU res sorted"<<endl;
//    
//    thrust::sort(gpu_idx, gpu_idx + list_size);
//    cout<<"   GPU res sorted"<<endl;
//    
//    err = compareArray(gpu_idx, cpu_idx, list_size);
//    cout<<"   sorted index list Checked"<<endl;
//    
//    delete[] cpu_idx; cpu_idx = NULL;
//    delete[] gpu_idx; gpu_idx = NULL;
//    
//    delete[] GType2_tmp; GType2_tmp = NULL;
//    return err;
//}

