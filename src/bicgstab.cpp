#include <cmath>
#include <cstring>
#include <iostream>
#include <omp.h>

using namespace std;

inline void init_value(int n,double val,double *a)
{
#   pragma omp parallel for
	for(int i=0;i<n;i++)
	{
		a[i]=val;
	}
}

inline void vectorp(int n,double *a,double *b,double *c)
{
#   pragma omp parallel for
	for(int i=0;i<n;i++)
	{
		c[i]=a[i]+b[i];
	}
}

inline void vectorm(int n,double *a,double *b,double *c)
{
#   pragma omp parallel for
	for(int i=0;i<n;i++)
	{
		c[i]=a[i]-b[i];
	}
}

inline void vector_scale(int n, double *a, double scale, double *b)
{
#   pragma omp parallel for
	for(int i=0;i<n;i++)
	{
		b[i]=scale*a[i];
	}
}

inline double vector_vector(int n, double *a,double *b)
{
	double result=0;
// parallelism will cause very small drift in output -- result, and hence the resultant bicg solution.
#   pragma omp parallel for reduction(+: result)
	for(int i=0;i<n;i++)
	{
		result=result+a[i]*b[i];
	}
	return result;
}

inline double norm2(int n, double *a)
{
	double result=0;
#   pragma omp parallel for reduction(+: result)
	for(int i=0;i<n;i++)
	{
		result=result+a[i]*a[i];
	}
	return (sqrt(result));
}

inline void vector_equal(int n, double *sour, double *dest)
{
#   pragma omp parallel for
	for(int i=0;i<n;i++)
	{
		dest[i]=sour[i];
	}
} 

// This routine performs the following vector operation:
//   destination  <--  alpha*x + y
inline void vector_axpy(int n, double* x, double alpha, double* y, double* dest)
{
#   pragma omp parallel for
	for(int i=0; i<n; i++)
	{
		dest[i] = alpha*x[i] + y[i];
	}
} 

void matv(int n, double *Axi, const double *xi, const unsigned int* coo_mark, 
    int nnz, const double* coo_value, const unsigned int* coo_row_idx, const unsigned int* coo_col_idx)
{
#   pragma omp parallel for 
    for (int i=0; i<n; i++)
    {
        Axi[i] = 0;
        for (int ijk=coo_mark[i]; ijk<coo_mark[i+1]; ijk++)
        {
            int j = coo_col_idx[ijk];
//            if (j > n)
//            {
//                cout << "WARNING: index exceeds array size in matv()!" << endl;
//                cout << "col: " << j << " row: " << coo_row_idx[ijk] << " vs " << i << endl;
//                continue;
//            }
//            if (coo_row_idx[ijk] != i)
//            {
//                cout << "WARNING: index pair not correct in matv()! " << endl;
//                cout << "col: " << j << " row: " << coo_row_idx[ijk] << " vs " << i << endl;
//                continue;
//            }
            Axi[i] += coo_value[ijk] * xi[j];
        }
    }
}

double bicgstab_solver
(
    int ns, double* xi, double* b, const unsigned int* coo_mark, 
    int nnz, const double* coo_value, const unsigned int* coo_row_idx, const unsigned int* coo_col_idx, 
    int maxit, double eps, int info
)
{
    int        iflag  = 1, icount = 0;
    double     delta0, deltai;
    double     bet, roi;
    double     roim1 = 1.0, al = 1.0, wi = 1.0;
	
    double *ri = new double [ns];
    double *t  = new double [ns];
    double *vi = new double [ns];
    double *pi = new double [ns];
	
    init_value(ns, 0.0, t);
    init_value(ns, 0.0, vi);
    init_value(ns, 0.0, pi);

    matv(ns, t, xi, coo_mark, nnz, coo_value, coo_row_idx, coo_col_idx);
    vectorm(ns, b, t, ri);
    vector_equal(ns, ri, b);
    delta0 = norm2(ns, ri);
    if (info == 1) cout << "initial error is " << delta0 << endl;
    if (delta0 < eps)
    {  
	    deltai = delta0;
    }
    else
    {
	    while ( iflag != 0 && icount < maxit ) 
	    {
	        icount += 1;
	        roi = vector_vector(ns, b, ri);
	        bet = (roi/roim1)*(al/wi);
	        
	        // pi[] = ri[] + bet*(pi[] - wi*vi[])
            vector_axpy(ns, vi, -1.0*wi, pi, pi);
            vector_axpy(ns, pi, bet, ri, pi);
            
            // vi[] = A[][] * pi[];
	        matv(ns, vi, pi, coo_mark, nnz, coo_value, coo_row_idx, coo_col_idx);  
	        
	        // al = roi / ( roc[] * vi[] );
	        al = roi/vector_vector(ns, b, vi);
	        
	        // s[] = ri[] - vi[] * al;   
	        vector_axpy(ns, vi, -1.0*al, ri, ri);
            
            // t[] = A[][] * s[];
	        matv(ns, t, ri, coo_mark, nnz, coo_value, coo_row_idx, coo_col_idx); 
	        
	        // wi = ( t[] * s[] ) / ( t[] * t[] );
	        wi = vector_vector(ns, t, ri)/vector_vector(ns, t, t);
	        
	        // xi[] += pi[] * al + s[] * wi;
	        vector_axpy(ns, ri, wi, xi, xi);
            vector_axpy(ns, pi, al, xi, xi);
            
	        // ri[] = s[] - t[] * wi;
	        vector_axpy(ns, t, -1.0*wi, ri, ri);
            
	        deltai = norm2(ns, ri);
	        
	        if(info==1) cout << "The " << icount << "th's error is " << deltai << endl;
	        
	        if( deltai < eps )
	        {
		        iflag = 0;
	        }
	        else
	        {
		        roim1 = roi;
	        }
	    }
    }
    
    if (info == 1) cout << "CPU | Bicgstab iteration: " << icount << " | ";
    
    delete []ri;
    delete []t;
    delete []vi;
    delete []pi;
    
    return deltai;
}

double bicgstab_solver2
(
    int ns, void (*matv)(int, double* xi, double* Axi),
    double* xi, double* b, int maxit, double eps, int info
)
{
    int        iflag  = 1, icount = 0;
    double     delta0, deltai;
    double     bet, roi;
    double     roim1 = 1.0, al = 1.0, wi = 1.0;
	
    double *ri = new double [ns];
    double *t = new double [ns];
    double *vi = new double [ns];
    double *pi = new double [ns];
	
    init_value(ns, 0.0, t);
    init_value(ns, 0.0, vi);
    init_value(ns, 0.0, pi);

    matv(ns, xi, t);
    vectorm(ns, b, t, ri);
    vector_equal(ns, ri, b);
    delta0 = norm2(ns, ri);
    if (info == 1) cout << "initial error is " << delta0 << endl;
    if (delta0 < eps)
    {  
	    deltai = delta0;
    }
    else
    {
	    while ( iflag != 0 && icount < maxit ) 
	    {
	        icount += 1;
	        roi = vector_vector(ns, b, ri);
	        bet = (roi/roim1)*(al/wi);
	        
	        // pi[] = ri[] + bet*(pi[] - wi*vi[])
            vector_axpy(ns, vi, -1.0*wi, pi, pi);
            vector_axpy(ns, pi, bet, ri, pi);
            
            // vi[] = A[][] * pi[];
	        matv(ns, pi, vi);  
	        
	        // al = roi / ( roc[] * vi[] );
	        al = roi/vector_vector(ns, b, vi);
	        
	        // s[] = ri[] - vi[] * al;   
	        vector_axpy(ns, vi, -1.0*al, ri, ri);
            
            // t[] = A[][] * s[];
	        matv(ns, ri, t);
	        
	        // wi = ( t[] * s[] ) / ( t[] * t[] );
	        wi = vector_vector(ns, t, ri)/vector_vector(ns, t, t);
	        
	        // xi[] += pi[] * al + s[] * wi;
	        vector_axpy(ns, ri, wi, xi, xi);
            vector_axpy(ns, pi, al, xi, xi);
            
	        // ri[] = s[] - t[] * wi;
	        vector_axpy(ns, t, -1.0*wi, ri, ri);
            
	        deltai = norm2(ns, ri);
	        
	        if(info==1) cout << "The " << icount << "th's error is " << deltai << endl;
	        
	        if( deltai < eps )
	        {
		        iflag = 0;
	        }
	        else
	        {
		        roim1 = roi;
	        }
	    }
    }
    
    if (info == 1) cout << "CPU | Bicgstab iteration: " << icount << " | ";
    
    delete []ri;
    delete []t;
    delete []vi;
    delete []pi;
    
    return deltai;
}

