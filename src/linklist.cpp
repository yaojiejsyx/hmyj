#include <math.h>
#include <omp.h>
#include <string.h>
#include <iostream>
#include <vector> 

#include "../common/mesh_type.h"

using namespace std;

//vector<int> listindex;
int FILTER=0;

extern vector<MeshlessMember> C2M_List;
extern char  *GType2, *GTypeT;
extern double *U, *V, *W, *P, *U_Old, *V_Old, *W_Old, *P_Old, *X, *Y, *Z;

void Copy_New2Old_C2M()
{
//    int i,io,j; 
//    #pragma omp parallel for private(io,j)
//    for(i=0;i<C2M_List.size();i++)
//    {
//	for(io=0;io<NB;io++)
//	{
//	    C2M_List[i].Nb_Points_Old[io]=C2M_List[i].Nb_Points[io];
//	    for(j=0;j<6;j++) C2M_List[i].Csvd_Old[j][io]=C2M_List[i].Csvd[j][io];
//	}
//    }
}

//void Insert_CarPoint(int i)
//{
//    MeshlessMember temp;
//    if(C2M_List.size()==0)
//    {
//	temp.Meshless_Ind = i;
//	C2M_List.push_back(temp);
//    }
//    else
//    {
//	if(FILTER==1)
//	{
//	    for(int s=0;s<C2M_List.size();s++)
//	    {
//		if(C2M_List[s].Meshless_Ind==i) 
//		{
//			cout<<"Link list point repeat."<<endl;
//			return;
//		}
//	    }
//	    temp.Meshless_Ind=i;
//	    C2M_List.push_back(temp);
//	}
//	else
//	{
//	    temp.Meshless_Ind=i;
//	    C2M_List.push_back(temp);
//	}
//    }
//}

//void Del_OldFSI()
//{ 
//    int i;
//    vector<MeshlessMember>::iterator itr=C2M_List.begin();
//    while(itr != C2M_List.end())
//    {
//	i=itr->Meshless_Ind;
//	if((GType2[i]==0||GType2[i]==1)&&GTypeT[i]==3)
//	{
//	    if(GType2[i]==0)
//	    {
//		U[i]=0;
//		V[i]=0;
//		W[i]=0;
//		P[i]=0;
//	    }
//	    itr=C2M_List.erase(itr);
//	}
//	else
//	{
//	    itr++;
//	}
//    }
//}

void Fresh(MeshlessMember *temp)
{
    int ijk=temp->Meshless_Ind;
    
    double tmp_u[OrderL] = {0}, tmp_v[OrderL] = {0}, tmp_w[OrderL] = {0};
    double tmp[OrderL] = {0}, tmp_p[OrderL] = {0};
    double drp[2], tmpu[2], tmpv[2], tmpw[2], tmpp[2];
    
    for (int od=0; od<OrderL; od++) {
        for (int io=0; io<NB; io++) {
            int j = temp->Nb_Points[io];
        
            tmp[od]   += temp->Csvd[od][io];
            tmp_u[od] += temp->Csvd[od][io]*U[j];
            tmp_v[od] += temp->Csvd[od][io]*V[j];
            tmp_w[od] += temp->Csvd[od][io]*W[j];
            tmp_p[od] += temp->Csvd[od][io]*P[j];
        }
    }
	
    for (int i=0; i<2; i++)
    {
        int ijk_p = temp->Nb_Points[i];
        double dx = X[ijk_p]-X[ijk];
        double dy = Y[ijk_p]-Y[ijk];
        double dz = Z[ijk_p]-Z[ijk];
        drp[i] = sqrt(dx*dx + dy*dy + dz*dz);

        double tmp_dr[OrderL] = { dx, dy, dz, 
                                 0.5*dx*dx, 0.5*dy*dy, 0.5*dz*dz,
                                 dx*dy, dx*dz, dy*dz };

        double dr = 1; 
        tmpu[i] = U[ijk_p]; tmpv[i] = V[ijk_p]; tmpw[i] = W[ijk_p];
        tmpp[i] = P[ijk_p];

        for (int od=0; od<OrderL; od++) {
            dr      -= tmp_dr[od]*tmp[od];
            tmpu[i] -= tmp_dr[od]*tmp_u[od];
            tmpv[i] -= tmp_dr[od]*tmp_v[od];
            tmpw[i] -= tmp_dr[od]*tmp_w[od];
            tmpp[i] -= tmp_dr[od]*tmp_p[od];
        }
        
        tmpu[i] /= dr;
        tmpv[i] /= dr;
        tmpw[i] /= dr;
        tmpp[i] /= dr;
    }
		
    U[ijk]=(drp[1]*tmpu[0]+drp[0]*tmpu[1])/(drp[0]+drp[1]);
    V[ijk]=(drp[1]*tmpv[0]+drp[0]*tmpv[1])/(drp[0]+drp[1]);
    W[ijk]=(drp[1]*tmpw[0]+drp[0]*tmpw[1])/(drp[0]+drp[1]);
    P[ijk]=(drp[1]*tmpp[0]+drp[0]*tmpp[1])/(drp[0]+drp[1]);
    U_Old[ijk]=U[ijk];
    V_Old[ijk]=V[ijk];
    W_Old[ijk]=W[ijk];
    P_Old[ijk]=P[ijk];
    //cout<<"fresh Point "<<ijk<<" :"<<U[ijk]<<" "<<V[ijk]<<" "<<W[ijk]<<" "<<P[ijk]<<endl;
}

void Fresh_Node()
{
    MeshlessMember *temp;
    int s;
#   pragma omp parallel for    
    for (s=0; s<C2M_List.size(); s++)
    {
        temp = &C2M_List[s];
        int ijk=temp->Meshless_Ind;
        if(GType2[ijk]==4)
        {
            Fresh(temp);
        }
    }
}

void Update_UT()
{
    MeshlessMember *temp;
    int s;
#   pragma omp parallel for    
    for (s=0; s<C2M_List.size(); s++)
    {
        temp = &C2M_List[s];
        int ijk=temp->Meshless_Ind;
        if ( (GType2[ijk]==1 && GTypeT[ijk]==0) 
            || (GType2[ijk]==3 && GTypeT[ijk]==0)
            || (GType2[ijk]==4) )
        {
            Fresh(temp);
        }
    }
}

