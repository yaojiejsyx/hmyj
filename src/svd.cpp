#include <vector> 
#include <omp.h>
#include <math.h>
#include <iostream>

using namespace std;

#include "../common/basis.h"
#include "../common/mesh_type.h"
#include "../common/timer.h"
#include "../common/f2c.h"
extern "C"
{
	#include "../common/clapack.h"
}

#define SVD_CUTOFF 1e-4
#define SVD_TOL 1e-16
#define MAX(a,b) ((a)>(b)?(a):(b))

//#define __SVD__
#ifdef __SVD__
#   define Order 19
#else
#   define Order 9
#endif /*__SVD__*/

static double sqrarg;
#define SQR(a) ((sqrarg=(a)) == 0.0 ? 0.0 : sqrarg*sqrarg)

static double dmaxarg1,dmaxarg2;
#define DMAX(a,b) (dmaxarg1=(a),dmaxarg2=(b),(dmaxarg1) > (dmaxarg2) ?\
(dmaxarg1) : (dmaxarg2))

static int iminarg1,iminarg2;
#define IMIN(a,b) (iminarg1=(a),iminarg2=(b),(iminarg1) < (iminarg2) ?\
(iminarg1) : (iminarg2))

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

extern vector<MeshlessMember> C2M_List;
extern MeshlessMember* Cld_List;
extern int      Point_Car, Point_Meshless;// **Nb_Points;
extern double   *X, *Y, *Z, Eff_Rad;// ***Csvd;
extern double   *U, *V, *W;

void svdcmp(double **a, int m, int n, double w[], double **v)
// NUMERICAL RECIPES code
//Given a matrix a[1..m][1..n], this routine computes its singular value decomposition, A =
//U . W . V^T . The matrix U replaces a on output. The diagonal matrix of singular values W is out-
//put as a vector w[1..n]. The matrix V (not the transpose V T ) is output as v[1..n][1..n].
{
	double pythag(double a, double b);
	int flag,i,its,j,jj,k,l,nm;
	double anorm,c,f,g,h,s,scale,x,y,z;

	double *rv1=new double [n]-1;
	g=scale=anorm=0.0; //Householder reduction to bidiagonal form.
	for (i=1;i<=n;i++) {
		l=i+1;
		rv1[i]=scale*g;
		g=s=scale=0.0;
		if (i <= m) {
			for (k=i;k<=m;k++) scale += fabs(a[k][i]);
			if (scale) {
				for (k=i;k<=m;k++) {
					a[k][i] /= scale;
					s += a[k][i]*a[k][i];
				}
				f=a[i][i];
				g = -SIGN(sqrt(s),f);
				h=f*g-s;
				a[i][i]=f-g;
				for (j=l;j<=n;j++) {
					for (s=0.0,k=i;k<=m;k++) s += a[k][i]*a[k][j];
					f=s/h;
					for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
				}
				for (k=i;k<=m;k++) a[k][i] *= scale;
			}
		}
		w[i]=scale *g;
		g=s=scale=0.0;
		if (i <= m && i != n) {
			for (k=l;k<=n;k++) scale += fabs(a[i][k]);
			if (scale) {
				for (k=l;k<=n;k++) {
					a[i][k] /= scale;
					s += a[i][k]*a[i][k];
				}
				f=a[i][l];
				g = -SIGN(sqrt(s),f);
				h=f*g-s;
				a[i][l]=f-g;
				for (k=l;k<=n;k++) rv1[k]=a[i][k]/h;
				for (j=l;j<=m;j++) {
					for (s=0.0,k=l;k<=n;k++) s += a[j][k]*a[i][k];
					for (k=l;k<=n;k++) a[j][k] += s*rv1[k];
				}
				for (k=l;k<=n;k++) a[i][k] *= scale;
			}
		}
		anorm=DMAX(anorm,(fabs(w[i])+fabs(rv1[i])));
	}
	for (i=n;i>=1;i--) {			//Accumulation of right-hand transformations.
		if (i < n) {
			if (g) {
				for (j=l;j<=n;j++)	//Double division to avoid possible under ow.
					v[j][i]=(a[i][j]/a[i][l])/g;
				for (j=l;j<=n;j++) {
					for (s=0.0,k=l;k<=n;k++) s += a[i][k]*v[k][j];
					for (k=l;k<=n;k++) v[k][j] += s*v[k][i];
				}
			}
			for (j=l;j<=n;j++) v[i][j]=v[j][i]=0.0;
		}
		v[i][i]=1.0;
		g=rv1[i];
		l=i;
	}
	for (i=IMIN(m,n);i>=1;i--) {	//Accumulation of left-hand transformations.
		l=i+1;
		g=w[i];
		for (j=l;j<=n;j++) a[i][j]=0.0;
		if (g) {
			g=1.0/g;
			for (j=l;j<=n;j++) {
				for (s=0.0,k=l;k<=m;k++) s += a[k][i]*a[k][j];
				f=(s/a[i][i])*g;
				for (k=i;k<=m;k++) a[k][j] += f*a[k][i];
			}
			for (j=i;j<=m;j++) a[j][i] *= g;
		} else for (j=i;j<=m;j++) a[j][i]=0.0;
		++a[i][i];
	}
	for (k=n;k>=1;k--) {		//Diagonalization of the bidiagonal form: Loop over
								//singular values, and over allowed iterations.
	for (its=1;its<=30;its++) {
		flag=1;
		for (l=k;l>=1;l--) {	//Test for splitting.
		nm=l-1;					//Note that rv1[1] is always zero.
		if ((double)(fabs(rv1[l])+anorm) == anorm) {
			flag=0;
			break;
		}
		if ((double)(fabs(w[nm])+anorm) == anorm) break;
	}
	if (flag) {
		c=0.0;					//Cancellation of rv1[l], if l > 1.
		s=1.0;
		for (i=l;i<=k;i++) {
			f=s*rv1[i];
			rv1[i]=c*rv1[i];
			if ((double)(fabs(f)+anorm) == anorm) break;
			g=w[i];
			h=pythag(f,g);
			w[i]=h;
			h=1.0/h;
			c=g*h;
			s = -f*h;
			for (j=1;j<=m;j++) {
				y=a[j][nm];
				z=a[j][i];
				a[j][nm]=y*c+z*s;
				a[j][i]=z*c-y*s;
			}
		}
	}
	z=w[k];
	if (l == k) {	//Convergence.
		if (z < 0.0) {	//Singular value is made nonnegative.
			w[k] = -z;
			for (j=1;j<=n;j++) v[j][k] = -v[j][k];
		}
		break;
	}
	if (its == 30) cout<<"no convergence in 30 svdcmp iterations"<<endl;
	x=w[l];				//Shift from bottom 2-by-2 minor.
	nm=k-1;
	y=w[nm];
	g=rv1[nm];
	h=rv1[k];
	f=((y-z)*(y+z)+(g-h)*(g+h))/(2.0*h*y);
	g=pythag(f,1.0);
	f=((x-z)*(x+z)+h*((y/(f+SIGN(g,f)))-h))/x;
	c=s=1.0;			//Next QR transformation:
	for (j=l;j<=nm;j++) {
		i=j+1;
		g=rv1[i];
		y=w[i];
		h=s*g;
		g=c*g;
		z=pythag(f,h);
		rv1[j]=z;
		c=f/z;
		s=h/z;
		f=x*c+g*s;
		g = g*c-x*s;
		h=y*s;
		y *= c;
		for (jj=1;jj<=n;jj++) {
			x=v[jj][j];
			z=v[jj][i];
			v[jj][j]=x*c+z*s;
			v[jj][i]=z*c-x*s;
		}
		z=pythag(f,h);
		w[j]=z;			//Rotation can be arbitrary if z = 0.
		if (z) {
			z=1.0/z;
			c=f*z;
			s=h*z;
		}
		f=c*g+s*y;
		x=c*y-s*g;
		for (jj=1;jj<=m;jj++) {
			y=a[jj][j];
			z=a[jj][i];
			a[jj][j]=y*c+z*s;
			a[jj][i]=z*c-y*s;
		}
	}
	rv1[l]=0.0;
	rv1[k]=f;
	w[k]=x;
	}
	}
	delete (rv1+1);
}

double pythag(double a, double b)
// NUMERICAL RECIPES code
//Computes (a^2 + b^2 )^1/2 without destructive under ow or over ow.
{
	double absa,absb;
	absa=fabs(a);
	absb=fabs(b);
	if (absa > absb) return absa*sqrt(1.0+SQR(absb/absa));
	else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb)));
}

int Csvd(MeshlessMember* Current_Meshless_Member)
{
    double **A, **VS, *WS, **CP, **US;
    int i, j, io, jo, ijo;
    double tmp1, tmp2, tmp3, Weight_Factor[NB], dr, DD, D_1[Order];
    double wmax, wmin;
    double upwscale_local=0.8;
    double umag_ale;

//    if(CheckForUpdateRequired_B(Current_Meshless_Member)==0) return;

    //Csvd[Order][NB][Type3_Number]
    //B.unit_matrix();
    //Meshless Cloud
    A = (new double* [NB])-1;
    for(i=1; i<=NB; i++) A[i] = (new double [Order]) - 1;

    US = (new double* [NB])-1;
    for(i=1; i<=NB; i++) US[i] = (new double [Order]) - 1;

    VS = (new double* [Order])-1;
    for(i=1; i<=Order; i++) VS[i] = (new double [Order]) - 1;


    WS = (new double [Order]) - 1;

    CP = (new double* [Order])-1;
    for(i=1; i<=Order; i++) CP[i] = (new double [NB]) - 1;

    i=Current_Meshless_Member->Meshless_Ind;

    umag_ale=sqrt(U[i]*U[i]+V[i]*V[i]+W[i]*W[i]);

    DD = 0;
    double DD_l;
    for(io=0;io<NB;io++) {
		j=Current_Meshless_Member->Nb_Points[io];
		tmp1=X[j]-X[i];
		tmp2=Y[j]-Y[i];
		tmp3=Z[j]-Z[i];
		DD_l = Eff_Rad*sqrt(tmp1*tmp1+tmp2*tmp2+tmp3*tmp3);
		DD = max(DD,DD_l);
		//DD += DD_l;
    }


    for(io=1; io<=NB; io++)
    {
        j=Current_Meshless_Member->Nb_Points[io-1];
        tmp1=(X[j]-X[i])/DD;
        tmp2=(Y[j]-Y[i])/DD;
        tmp3=(Z[j]-Z[i])/DD;

        dr = sqrt(tmp1*tmp1+tmp2*tmp2+tmp3*tmp3);
        if(dr<=0.5)
        {
            Weight_Factor[io-1]=2.0/3.0-4.0*dr*dr+4.0*dr*dr*dr;
        }
        else if(dr<=1)
        {
            Weight_Factor[io-1]=4.0/3.0-4.0*dr+4.0*dr*dr-4.0*dr*dr*dr/3.0;
        }
        else
        {
            Weight_Factor[io-1]=0;
        }

//#ifdef UPW_WEIGHTING
//        //upwind formulation
//        Weight_Factor[io - 1] *= upwscale_local
//        		- (1 - upwscale_local)
//        		* (tmp1 * (U[i] )
//        				+ tmp2 * (V[i])
//        				+ tmp3 * (W[i]))
//        				/ (dr * Eff_Rad * (umag_ale + 1e-3));
//#endif

        A[io][1]=tmp1*Weight_Factor[io-1];
        A[io][2]=tmp2*Weight_Factor[io-1];
        A[io][3]=tmp3*Weight_Factor[io-1];
        A[io][4]=tmp1*tmp1*0.5*Weight_Factor[io-1];
        A[io][5]=tmp2*tmp2*0.5*Weight_Factor[io-1];
        A[io][6]=tmp3*tmp3*0.5*Weight_Factor[io-1];
        A[io][7]=tmp1*tmp2*Weight_Factor[io-1];
        A[io][8]=tmp1*tmp3*Weight_Factor[io-1];
        A[io][9]=tmp2*tmp3*Weight_Factor[io-1];
        A[io][10]=tmp1*tmp1*tmp1/6.0*Weight_Factor[io-1];
        A[io][11]=tmp2*tmp2*tmp2/6.0*Weight_Factor[io-1];
        A[io][12]=tmp3*tmp3*tmp3/6.0*Weight_Factor[io-1];
        A[io][13]=tmp1*tmp2*tmp2*0.5*Weight_Factor[io-1];
        A[io][14]=tmp1*tmp3*tmp3*0.5*Weight_Factor[io-1];
        A[io][15]=tmp2*tmp1*tmp1*0.5*Weight_Factor[io-1];
        A[io][16]=tmp3*tmp1*tmp1*0.5*Weight_Factor[io-1];
        A[io][17]=tmp2*tmp3*tmp3*0.5*Weight_Factor[io-1];
        A[io][18]=tmp3*tmp2*tmp2*0.5*Weight_Factor[io-1];
        A[io][19]=tmp1*tmp2*tmp3*Weight_Factor[io-1];
    }

    for(io=1; io<=NB; io++)
        for(jo=1; jo<=OrderL; jo++)
        {
            US[io][jo]=A[io][jo];
        }
    svdcmp(US, NB, OrderL, WS, VS);
//    D_1[0]=1.0;
    D_1[0]=1.0/DD;
    D_1[1]=D_1[0];
    D_1[2]=D_1[0];
//    D_1[3]=1.0;
    D_1[3]=1.0/DD/DD;
    D_1[4]=D_1[3];
    D_1[5]=D_1[3];
    D_1[6]=D_1[3];
    D_1[7]=D_1[3];
    D_1[8]=D_1[3];
////    D_1[9]=1.0;
    D_1[9]=1.0/DD/DD/DD;
    D_1[10]=D_1[9];
    D_1[11]=D_1[9];
    D_1[12]=D_1[9];
    D_1[13]=D_1[9];
    D_1[14]=D_1[9];
    D_1[15]=D_1[9];
    D_1[16]=D_1[9];
    D_1[17]=D_1[9];
    D_1[18]=D_1[9];
    wmax=0.0;
    wmin = WS[1];
    for(jo=1; jo<=OrderL; jo++) {
    	if(WS[jo]>wmax) wmax=WS[jo];
    	wmin = min(wmin,WS[jo]);
    }

//    condnum[i] = wmax/(wmin+1e-10);

    wmin=wmax*SVD_CUTOFF;
    for(jo=1; jo<=OrderL; jo++) if(WS[jo]<wmin) WS[jo]=0;

    for(jo=1; jo<=OrderL; jo++)
    {
        if(WS[jo]!=0) WS[jo]=1.0/WS[jo];
    }
    for(io=1; io<=OrderL; io++)
        for(jo=1; jo<=OrderL; jo++)
        {
            VS[io][jo]=VS[io][jo]*WS[jo];
        }
    for(io=1; io<=NB; io++)
        for(jo=1; jo<=OrderL; jo++)
        {
            CP[jo][io]=0;
            for(ijo=1; ijo<=OrderL; ijo++) CP[jo][io]+=VS[jo][ijo]*US[io][ijo];
        }
    for(io=1; io<=NB; io++)
        for(jo=1; jo<=3; jo++)
        {
            Current_Meshless_Member->Csvd[jo-1][io-1]=Weight_Factor[io-1]*D_1[jo-1]*CP[jo][io];
        }

    svdcmp(A, NB, Order, WS, VS);

    wmax=0.0;
    for(jo=1; jo<=Order; jo++) if(WS[jo]>wmax) wmax=WS[jo];
    wmin=wmax*SVD_CUTOFF;
    for(jo=1; jo<=Order; jo++) if(WS[jo]<wmin) WS[jo]=0;
    
    int s_cnt = 0;
    for(jo=1; jo<=Order; jo++)
    {
        if(WS[jo]!=0)
        {
            WS[jo]=1.0/WS[jo];
            s_cnt++;
        }
    }
    for(io=1; io<=Order; io++)
        for(jo=1; jo<=Order; jo++)
        {
            VS[io][jo]=VS[io][jo]*WS[jo];
        }
    for(io=1; io<=NB; io++)
        for(jo=1; jo<=Order; jo++)
        {
            CP[jo][io]=0;
            for(ijo=1; ijo<=Order; ijo++) CP[jo][io]+=VS[jo][ijo]*A[io][ijo];
        }
    for(io=1; io<=NB; io++)
        for(jo=4; jo<=9; jo++)
        {
            Current_Meshless_Member->Csvd[jo-1][io-1]=Weight_Factor[io-1]*D_1[jo-1]*CP[jo][io];
        }

    for(i=1; i<=NB; i++) delete (A[i]+1);
    delete (A+1);

    for(i=1; i<=NB; i++) delete (US[i] +1);
    delete (US+1);

    for(i=1; i<=Order; i++) delete (VS[i] +1);
    delete (VS+1);

    delete (WS+1);

    for(i=1; i<=Order; i++) delete (CP[i] +1);
    delete (CP+1);
    
    if (s_cnt<OrderL) return 1;
    
    return 0;
}

int Cwls(MeshlessMember *Current_Meshless_Member)
{
    double *S_svd, *W_svd; //, *dst;
    S_svd = new double [NB*Order];
    W_svd = new double [NB];
    
    double **STWS_svd;
    STWS_svd=new double* [Order]; for(int ii=0; ii<Order; ii++) STWS_svd[ii] = new double [Order];
    
    unsigned i = Current_Meshless_Member->Meshless_Ind, j, s_cnt;
    double delta_x, delta_y, delta_z, s0;
    
    // compute distance to each node
	double dst_max = 0;
	for (int s=0; s<NB; s++)
	{
	    j = Current_Meshless_Member->Nb_Points[s];
	    
	    delta_x = X[j]-X[i];
	    delta_y = Y[j]-Y[i];
	    delta_z = Z[j]-Z[i];
	    
	    W_svd[s] = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
	    dst_max = MAX(dst_max, Eff_Rad * W_svd[s]);
	    
	    // construct the NB*Order coefficient matrix S
		S_svd[s + 0*NB] = delta_x;
		S_svd[s + 1*NB] = delta_y;
		S_svd[s + 2*NB] = delta_z;
		S_svd[s + 3*NB] = delta_x*delta_x/2.0;
		S_svd[s + 4*NB] = delta_y*delta_y/2.0;
		S_svd[s + 5*NB] = delta_z*delta_z/2.0;
		S_svd[s + 6*NB] = delta_x*delta_y;
		S_svd[s + 7*NB] = delta_x*delta_z;
		S_svd[s + 8*NB] = delta_y*delta_z;
/*		
		S_svd[s + 9*NB]  = delta_x*delta_x*delta_x/6.0;
		S_svd[s + 10*NB] = delta_y*delta_y*delta_y/6.0;
		S_svd[s + 11*NB] = delta_z*delta_z*delta_z/6.0;
		S_svd[s + 12*NB] = delta_x*delta_x*delta_y/2.0;
		S_svd[s + 13*NB] = delta_x*delta_x*delta_z/2.0;
		S_svd[s + 14*NB] = delta_y*delta_y*delta_x/2.0;
		S_svd[s + 15*NB] = delta_y*delta_y*delta_z/2.0;
		S_svd[s + 16*NB] = delta_z*delta_z*delta_x/2.0;
		S_svd[s + 17*NB] = delta_z*delta_z*delta_y/2.0;
		S_svd[s + 18*NB] = delta_x*delta_y*delta_z;
*/
	}
	
	// assign weighting function
	for (int ii=0; ii<NB; ii++)
	{
		double dr = W_svd[ii]/dst_max;
		
		if (dr <= 0.05) 
		{
			W_svd[ii] = 0;
		}
		else if (dr <= 0.5) 
		{
			W_svd[ii] = 2.0/3.0 - 4.0*dr*dr + 4.0*dr*dr*dr;
		}
		else if(dr <= 1)
		{
			W_svd[ii] = 4.0/3.0 - 4.0*dr + 4.0*dr*dr - 4.0*dr*dr*dr/3.0;
		}
		else
		{
			W_svd[ii] = 0;
		}
	}
	
	// compute S'*W*S
	for (int ii=0; ii<Order; ii++)
    for (int jj=0; jj<Order; jj++)
    {
        STWS_svd[ii][jj] = 0;
        for (int kk=0; kk<NB; kk++)
        {
            STWS_svd[ii][jj] += S_svd[kk + ii*NB]*W_svd[kk]*S_svd[kk + jj*NB];
        }
    }
	
	dcinv(STWS_svd, Order);
	
	// compute [S'*W*S]^(-1)*S'*W
	for (int ii=0; ii<9; ii++)
	for (int jj=0; jj<NB; jj++)
	{
		Current_Meshless_Member->Csvd[ii][jj] = 0;
        for (int kk=0; kk<Order; kk++)
        {
            Current_Meshless_Member->Csvd[ii][jj] += STWS_svd[ii][kk]*S_svd[jj + kk*NB]*W_svd[jj];
        }
	}
	
	delete[] W_svd;
	delete[] S_svd;
	for (int ii=0; ii<Order; ii++) { delete[] STWS_svd[ii]; } 
	delete[] STWS_svd;
	
	return 0;
}

int Csvd_lapack(MeshlessMember* Current_Meshless_Member);

void Calculate_SVD(int wi)
{
    int s, err=0;
    
#ifdef  __SVD__
#	pragma omp parallel for reduction(+:err)
	for (s=0; s<Point_Meshless; s++)
	{
        err += Csvd(&Cld_List[s]);
	}
	if (err>0) printf("*** Bad quality nodes in cloud: %d/%d ***\n", err, Point_Meshless);
	err = 0;
#	pragma omp parallel for reduction(+:err)
    for (s=0; s<C2M_List.size(); s++)
    {
        err += Csvd(&C2M_List[s]);
    }
    if (err>0) printf("*** Bad quality nodes in c2m list: %d/%d ***\n", err, int(C2M_List.size()));
#else
#	pragma omp parallel for
	for (s=0; s<Point_Meshless; s++)
	{
        Cwls(&Cld_List[s]);
	}
#	pragma omp parallel for
    for (s=0; s<C2M_List.size(); s++)
    {
        Cwls(&C2M_List[s]);
    }
#endif /*__SVD__*/
}


// c++ interface for LAPACK svd routine (handle a martix A of size M*N, M>=N>=1)
// notice: lapack stores 2-D array in column-major order
int svdcmp_lapack(double *a, int M, int N, double *s, double *u, double *vt)
{
	long int m, n, lda, ldu, ldvt, lwork, info;
	m = M; n = N;
	
    char jobz='O'; // 'O' for overwrite, use A to store first min(m,n) columns of U
    lda=M; ldu=M; ldvt=N;
    lwork = 3*min(m,n) + max(max(m,n), 5*min(m,n)*min(m,n)+4*min(m,n));
//    lwork = 3*N + 5*N*N + 4*N;
    double *work=new double [lwork];
    long int *iwork=new long int [8*N]; //8*min(m,n)

    dgesdd_(&jobz, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, work, &lwork, iwork, &info);

    delete[] work;
    delete[] iwork;
    
    
//    char jobu='O', jobvt = 'A';
//    lda=M; ldu=M; ldvt=N;
//    lwork = max(3*min(m,n)+max(m,n), 5*min(m,n))+32;
//    double *work=new double [lwork];
//    
//    dgesvd_(&jobu, &jobvt, &m, &n, a, &lda, s, u, &ldu, vt, &ldvt, work, &lwork, &info);
//    
//    delete[] work;
    
	return info;
}

int Csvd_lapack(MeshlessMember* Current_Meshless_Member)
{
    // all matrix should be stored in column-major order
    double *S_svd, *W_svd; //, *dst;
    double *Sigma_svd, *VT_svd;
//    double *U_svd, *V_svd;
//    dst = new double [NB];
    S_svd = new double [NB*Order];
    W_svd = new double [NB];
    
    Sigma_svd = new double [Order];
//    U_svd = new double [NB*Order];
    VT_svd = new double [Order*Order];
//    V_svd = new double [Order*Order];
    
    unsigned i = Current_Meshless_Member->Meshless_Ind, j, s_cnt;
    double delta_x, delta_y, delta_z, s0;
    
    // compute distance to each node
	double dst_max = 0;
	for (int s=0; s<NB; s++)
	{
	    j = Current_Meshless_Member->Nb_Points[s];
	    
	    delta_x = X[j]-X[i];
	    delta_y = Y[j]-Y[i];
	    delta_z = Z[j]-Z[i];
	    
	    W_svd[s] = sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z);
	    dst_max = MAX(dst_max, Eff_Rad * W_svd[s]);
	    
	    // construct the NB*Order coefficient matrix S
		S_svd[s + 0*NB] = delta_x;
		S_svd[s + 1*NB] = delta_y;
		S_svd[s + 2*NB] = delta_z;
		S_svd[s + 3*NB] = delta_x*delta_x/2.0;
		S_svd[s + 4*NB] = delta_y*delta_y/2.0;
		S_svd[s + 5*NB] = delta_z*delta_z/2.0;
		S_svd[s + 6*NB] = delta_x*delta_y;
		S_svd[s + 7*NB] = delta_x*delta_z;
		S_svd[s + 8*NB] = delta_y*delta_z;
		
		S_svd[s + 9*NB]  = delta_x*delta_x*delta_x/6.0;
		S_svd[s + 10*NB] = delta_y*delta_y*delta_y/6.0;
		S_svd[s + 11*NB] = delta_z*delta_z*delta_z/6.0;
		S_svd[s + 12*NB] = delta_x*delta_x*delta_y/2.0;
		S_svd[s + 13*NB] = delta_x*delta_x*delta_z/2.0;
		S_svd[s + 14*NB] = delta_y*delta_y*delta_x/2.0;
		S_svd[s + 15*NB] = delta_y*delta_y*delta_z/2.0;
		S_svd[s + 16*NB] = delta_z*delta_z*delta_x/2.0;
		S_svd[s + 17*NB] = delta_z*delta_z*delta_y/2.0;
		S_svd[s + 18*NB] = delta_x*delta_y*delta_z;
	}
	
	// assign weighting function, construct matrix W*S
	for (int ii=0; ii<NB; ii++)
	{
		double dr = W_svd[ii]/dst_max;
		
		if (dr <= 0.5) 
		{
			W_svd[ii] = 2.0/3.0 - 4.0*dr*dr + 4.0*dr*dr*dr;
		}
		else if(dr <= 1)
		{
			W_svd[ii] = 4.0/3.0 - 4.0*dr + 4.0*dr*dr - 4.0*dr*dr*dr/3.0;
		}
		else
		{
			W_svd[ii] = 0;
		}
		
	    for (int jj=0; jj<Order; jj++)
	    {
	        S_svd[ii + jj*NB] *= W_svd[ii];
	    }
	}
	
	int err = svdcmp_lapack(S_svd, NB, Order, Sigma_svd, S_svd, VT_svd);
	if (err != 0) cout << "*** ERR: SVD not done ***\n";
	
    s0 = Sigma_svd[0]; // maximum singular value
    s_cnt = 0;
    for (int ii=0; ii<Order; ii++)
    {
        if (Sigma_svd[ii] < s0*SVD_CUTOFF) { Sigma_svd[ii]=0; }

        if (Sigma_svd[ii] > SVD_TOL) 
        {
            Sigma_svd[ii] = 1.0/Sigma_svd[ii];
            s_cnt++;
        }
        else
        {
            Sigma_svd[ii] = 0;
        }
    }
	
	// compute V*[SIGMA+]*U'*W
    for (int ii=0; ii<9; ii++) // row ii
    for (int jj=0; jj<NB; jj++) // column jj
    {
        double tmp = 0;
        
        // V[ii][jj] = VT[jj][ii]
        // UT[kk][jj] = U[jj][kk], U is stored in S_svd
        for (int kk=0; kk<Order; kk++)
            tmp += VT_svd[kk + ii*Order]*Sigma_svd[kk]*S_svd[jj + kk*NB];
//            tmp[ii][jj] += V_svd[ii + kk*Order]*S_svd[jj + kk*NB];
        
        Current_Meshless_Member->Csvd[ii][jj] = tmp*W_svd[jj];
	}
	
//	delete[] dst;
	delete[] S_svd;
	delete[] W_svd;
	
	delete[] Sigma_svd;
//	delete[] U_svd;
	delete[] VT_svd;
//	delete[] V_svd;
	
	if (s_cnt < OrderL) return 1;
	
	return 0;
}

