#include <fstream>
#include <iostream>

using namespace std;

#define  PI 3.1415926535897932

#include "../common/flap_pattern.h"
#include "../common/basis.h"


//
//  current body angle configuration
//  body_centre[3]: 0 --> x,     1 --> y,           2 --> z
//  body_angle[3]:  0 --> yaw,   1 --> roll,        2 --> pitch
//

void FlapPattern :: Init_Flap_Controllers(void)
{
    X_controller.name="X_controller";
    X_controller.set=0.0;
    X_controller.Kp=12.5*PI/9.0;
    X_controller.Ki=10.0;
    X_controller.Kd=50.0;
    X_controller.epsilon=0.005;
    X_controller.Init=0;
    X_controller.Max=5.0*PI/180.0;
    X_controller.Min=-5.0*PI/180.0;
    
    
    Y_controller.name="Y_controller";
    Y_controller.set=0.0;
    Y_controller.Kp=-0.3;
    Y_controller.Ki=-0.01; 
    Y_controller.Kd=-2.0;
    Y_controller.epsilon=5;
    Y_controller.Init=0.5*PI/180.0;
    Y_controller.Max=20.0*PI/180.0;
    Y_controller.Min=-20.0*PI/180.0;
    
    
    Z_controller.name="Z_controller";
    Z_controller.set=8.0;
    Z_controller.Kp=50.0;
    Z_controller.Ki=10.0;
    Z_controller.Kd=200.0;
    Z_controller.epsilon=0.005;
    Z_controller.Init=65.0;
    Z_controller.Max=70.0;
    Z_controller.Min=60.0;
    
    
    V_controller.name="V_controller";
    V_controller.Kp=-25.0*PI/9.0; 
    V_controller.Ki=-0;
    V_controller.Kd=-2;
    V_controller.epsilon=0.002;
    V_controller.Init=0;
    V_controller.Max=5.0*PI/180.0;
    V_controller.Min=-5.0*PI/180.0;

    
    Orientation_controller.name="Orientation_controller";
    Orientation_controller.set=0*PI/180;
    Orientation_controller.Kp=0.1;
    Orientation_controller.Ki=0.01;
    Orientation_controller.Kd=0.7;
    Orientation_controller.epsilon=180.0*PI/180.0;
    Orientation_controller.Init=-7*PI/180.0;
    Orientation_controller.Max=0*PI/180.0;
    Orientation_controller.Min=-12*PI/180.0;
	//Orientation_controller.Max_Change=5.0*PI/180;
	

	Roll_controller.name="Roll_controller";
    Roll_controller.set=0.0*PI/180;
    Roll_controller.Kp=0.4;
    Roll_controller.Ki=0.1;
    Roll_controller.Kd=1.0;
    Roll_controller.epsilon=2.0*PI/180.0;
    Roll_controller.Init=0*PI/180.0;
    Roll_controller.Max=3.0*PI/180.0;
    Roll_controller.Min=-3.0*PI/180.0;
    
    
    Yaw_controller.name="Yaw_controller";
    Yaw_controller.set=0.0*PI/180;
    Yaw_controller.Kp=-0.3;
    Yaw_controller.Ki=0;
    Yaw_controller.Kd=-0.4;
    Yaw_controller.epsilon=1.0*PI/180.0;
    Yaw_controller.Init=0.0*PI/180.0;
    Yaw_controller.Max=3.0*PI/180.0;
    Yaw_controller.Min=-3.0*PI/180.0;
}

void FlapPattern :: Update_Flap_Controllers(double body_centre[3], double body_angle[3], double body_centre_old[3], double pitch_ave)
{

	double x_aim = 8.0, y_aim = 8.0;
	double x_delta = x_aim - body_centre[0], y_delta = y_aim - body_centre[1];
	double current_dist = sqrt(x_delta*x_delta + y_delta*y_delta);
	
	double x_input = -(x_delta*cos(-body_angle[0])-y_delta*sin(-body_angle[0]));
	double y_input = -(x_delta*sin(-body_angle[0])+y_delta*cos(-body_angle[0]));
	double v_input = ((body_centre[0]-body_centre_old[0])*sin(-body_angle[0])
					+(body_centre[1]-body_centre_old[1])*cos(-body_angle[0])) * freq_old/freq_global;     // longitudinal velocity
	/*
	double yaw_input = atan2(x_delta, y_delta) + insect->angle[0];
	if(current_dist < 20*maxD) {
		if(fabs(yaw_input) > 2.0/3.0*PI) yaw_input = (yaw_input > 0) ? (yaw_input - PI) : (yaw_input + PI);
		if(fabs(v_input) > maxV || current_dist < maxD) yaw_input = 0;
		if(current_dist<maxD && fabs(v_input)<maxV) yaw_input = insect->angle[0];
	}
	*/
	double yaw_input   = body_angle[0]; // double yaw_input = insect->angle[0];
	// for forward flight
//	double yaw_input   = atan2(x_delta, y_delta) + body_angle[0];
	double roll_input  = body_angle[1];
	double pitch_input = body_angle[2];
	
	double dt = freq_global/freq_old;
	
//  longitudinal position and velocity controller, corresponding to wingplane angle PSI -- Angle_*_Psi
	double maxD=0.1, maxV=0.1;
	// if(current_dist > 20*maxD) maxV = 0.2;
	// if(y_input>maxD) {
		// if(v_input < -maxV) {
			// V_controller.set=0.0; Angle_Next_Psi = V_controller.Activate_controller(v_input, dt);
			// Y_controller.Activate_controller(y_input, dt);
		// }
		// else {
			// V_controller.set=-maxV; Angle_Next_Psi = V_controller.Activate_controller(v_input, dt);
			// Y_controller.Activate_controller(y_input, dt);
		// }
	// }
	// else if(y_input<-maxD) {
		// if(v_input > maxV) {
			// V_controller.set=0.0; Angle_Next_Psi = V_controller.Activate_controller(v_input, dt);
			// Y_controller.Activate_controller(y_input, dt);
		// }
		// else {
			// V_controller.set=maxV; Angle_Next_Psi = V_controller.Activate_controller(v_input, dt);
			// Y_controller.Activate_controller(y_input, dt);
		// }
	// }
	// else if(fabs(y_input)<=maxD) {
		// if(fabs(v_input) > maxV) {
			// V_controller.set=0.0; Angle_Next_Psi = V_controller.Activate_controller(v_input, dt);
			// Y_controller.Activate_controller(y_input, dt);
		// }
		// else {
			// V_controller.set=0.0; V_controller.Activate_controller(v_input, dt);
			// Angle_Next_Psi = Y_controller.Activate_controller(y_input, dt);
		// }
	// }
	// else {
		// cout<<"Mistake: beyond control plan....."<<endl;
	// }
//    Angle_Next_Psi_Delta = -Angle_Next_Psi;
    Angle_Next_Psi_Delta = 0;
 			Angle_Next_Psi = Y_controller.Activate_controller(y_input, dt) - pitch_ave;   
//  lateral position controller, corresponding to wingplane angle THETA -- Angle_*_Theta
//	int x_ctrl_interval = 20;
//	if(X_controller.record.size() % x_ctrl_interval==0) {
//		if(x_input>0.05) {
//			Angle_Next_Theta = X_controller.Max;
//			X_controller.record.push_back(1);
//		}
//		else if(x_input<-0.05) {
//			Angle_Next_Theta = X_controller.Min;
//			X_controller.record.push_back(1);
//		}
//		else {
//			Angle_Next_Theta = 0;
//			X_controller.record.push_back(-1);
//		}
//	}
//	else if(X_controller.record.size() % x_ctrl_interval < (x_ctrl_interval/2)) {
//		Angle_Next_Theta = Angle_Current_Theta;
//		X_controller.record.push_back(1);
//	}
//	else {
//		Angle_Next_Theta = 0;
//		X_controller.record.push_back(-1);
//	}


//  pitch angle controller, corresponding to wingplane mean position angle PHI -- Angle_*_Phi
	Angle_Next_Phi=Orientation_controller.Activate_controller(pitch_input, dt);
	Angle_Next_Phi=asin(sin(Angle_Next_Phi)*cos(Angle_Next_Psi)-0.41*sin(Angle_Next_Psi));
	// Angle_Next_Phi=Orientation_controller.Init;
   // Angle_Next_Phi=Orientation_controller.Activate_controller(pitch_input, dt) - 0.15*Angle_Next_Psi; 
   
//  yaw angle controller, corresponding to change in wing angle of attack -- Angle_*_AoA_Delta
//  roll angle controller, corresponding to change in wing stroke amplitude -- Angle_*_Stroke_Delta

    X_controller.record.push_back(x_input);
    if(X_controller.record.size()>5) {
        double lateral_drift = X_controller.record[X_controller.record.size()-1] - X_controller.record[X_controller.record.size()-6];
        if(fabs(lateral_drift) > 0.005 || fabs(x_input) > 0.01) {
            Roll_controller.set = ( -100*x_input-25.0*lateral_drift ) * PI/180;
            Roll_controller.set = (fabs(Roll_controller.set) > 5*PI/180) ? 
                                    5*PI/180*sgn(Roll_controller.set) : Roll_controller.set;
        }
        else {
            Roll_controller.set = 0;
        }
    }
	
	double Angle_tmp_AoA_Delta, Angle_tmp_Stroke_Delta;
	Angle_tmp_Stroke_Delta=Roll_controller.Activate_controller(roll_input, dt);
	Angle_tmp_AoA_Delta=Yaw_controller.Activate_controller(yaw_input, dt);
	Angle_Next_Stroke_Delta=Angle_tmp_Stroke_Delta+0.75*Angle_tmp_AoA_Delta;
	Angle_Next_AoA_Delta=Angle_tmp_AoA_Delta+0.68*Angle_tmp_Stroke_Delta;	
}

// This function will be execute every time step
void FlapPattern :: Update_Flap_Pattern(double stime, int it, double body_centre[3], double body_angle[3])
{
    bool is_new = false;
	double past_pitch_ave;
    
    // check if new cycle begins
    if (stime >= stime_end) {
        is_new = true;
        stime_start = stime_end;
        freq_old = freq;
        cyc_counter++;
    }
    
		
//    if(it%check_intvl_Angle==0 && modify_mark[int(it/check_intvl_Angle)]==0)
    if (is_new)
    {
		double sum=0;
		for (int i=0;i<Orientation_controller.past_cycle_record.size();i++)
            {
                sum += Orientation_controller.past_cycle_record[i];
            }
		past_pitch_ave=sum/Orientation_controller.past_cycle_record.size();
		
//        modify_mark[int(it/check_intvl_Angle)]=1;
        freq = Z_controller.Activate_controller(body_centre[2], freq_global/freq_old);
//        freq = freq_global + 20*cos(PI/2*total_cyc_time);
        stime_end = stime_start + freq_global/freq;
        
        Angle_Previous_Psi = Angle_Current_Psi;
        Angle_Previous_Theta = Angle_Current_Theta;
        Angle_Previous_Phi = Angle_Current_Phi;
        
        Angle_Previous_AoA_Delta = Angle_Current_AoA_Delta;
        Angle_Previous_Stroke_Delta = Angle_Current_Stroke_Delta;
        Angle_Previous_Psi_Delta = Angle_Current_Psi_Delta;
        
        Update_Flap_Controllers(body_centre, body_angle, lastbodycentre, past_pitch_ave);
        
        // Manually set controlling angles
		// Angle_Next_Phi = -4.5*PI/180.0;
        // Angle_Next_Psi = 0.0*PI/180.0;
        Angle_Next_Theta = 0.0*PI/180.0;
        Angle_Next_AoA_Delta = 0.0*PI/180.0;
        Angle_Next_Stroke_Delta = 0.0*PI/180.0;
        Angle_Next_Psi_Delta = 0.0*PI/180.0;
        
        Update_Flap_Kinematics();
        
        for(int i=0;i<3;i++)
	        lastbodycentre[i] = body_centre[i];	
    }
	
	Orientation_controller.Sum_past_cycle(body_angle[2], is_new);
    
    cyc_time = (stime - stime_start) * freq/freq_global;
    if (cyc_time > 1.0) {
        cout<<"ERROR: flap cycle not updated. stime = "<<stime<<", ctime = "<<cyc_time<<endl;
    }
    total_cyc_time = float(cyc_counter) + cyc_time;
    
	Angle_Current_Phi   = wingplane1_Phi(stime, 0);
	Angle_Current_Theta = wingplane1_Theta(stime, 0);
	Angle_Current_Psi   = wingplane1_Psi(stime, 0);

//	if(fabs(cos(2*PI*cyc_time)) > 0.001)
//		Angle_Current_Stroke_Delta = (wing1_Phi(stime, 0) + wing2_Phi(stime, 0))/cos(2*PI*cyc_time)/2.0;
	Angle_Current_Stroke_Delta = Angle_Next_Stroke_Delta;
	Angle_Current_AoA_Delta = Angle_Next_AoA_Delta;
	Angle_Current_Psi_Delta = Angle_Next_Psi_Delta;
}

int FlapPattern :: Load_Flap_Pattern(ifstream &in_info)
{
	if(!in_info.is_open()) {
		cout<<"Unable to open Motion Record File!"<<endl;
		return 1;
	}
	
	in_info>>cyc_counter>>total_cyc_time>>cyc_time
	       >>stime_start>>stime_end;
	
	// controller
	in_info>>freq>>freq_old>>freq_global;
	
	in_info>>Angle_Previous_Phi         
	       >>Angle_Current_Phi         
	       >>Angle_Next_Phi;
	in_info>>Angle_Previous_Theta       
	       >>Angle_Current_Theta
	       >>Angle_Next_Theta;
	in_info>>Angle_Previous_Psi
	       >>Angle_Current_Psi
	       >>Angle_Next_Psi;
	in_info>>Angle_Previous_Stroke_Delta
	       >>Angle_Current_Stroke_Delta
	       >>Angle_Next_Stroke_Delta;
	in_info>>Angle_Previous_AoA_Delta   
	       >>Angle_Current_AoA_Delta
	       >>Angle_Next_AoA_Delta;
	in_info>>Angle_Previous_Psi_Delta   
	       >>Angle_Current_Psi_Delta
	       >>Angle_Next_Psi_Delta;
	
    int count = 6, size; 
    double         para_record[count];
    vector<double> output_record;
    
    for(int i=0;i<count;i++) in_info>>para_record[i];
    in_info>>size; output_record.resize(size);
    for(int i=0;i<size;i++)  in_info>>output_record[i];
    X_controller.Evoke_controller(para_record, output_record);

    for(int i=0;i<count;i++) in_info>>para_record[i];
    in_info>>size; output_record.resize(size);
    for(int i=0;i<size;i++)  in_info>>output_record[i];
    Y_controller.Evoke_controller(para_record, output_record);

    for(int i=0;i<count;i++) in_info>>para_record[i];
    in_info>>size; output_record.resize(size);
    for(int i=0;i<size;i++)  in_info>>output_record[i];
    Z_controller.Evoke_controller(para_record, output_record);

    for(int i=0;i<count;i++) in_info>>para_record[i];
    in_info>>size; output_record.resize(size);
    for(int i=0;i<size;i++)  in_info>>output_record[i];
    V_controller.Evoke_controller(para_record, output_record);

    for(int i=0;i<count;i++) in_info>>para_record[i];
    in_info>>size; output_record.resize(size);
    for(int i=0;i<size;i++)  in_info>>output_record[i];
    Orientation_controller.Evoke_controller(para_record, output_record);

    for(int i=0;i<count;i++) in_info>>para_record[i];
    in_info>>size; output_record.resize(size);
    for(int i=0;i<size;i++)  in_info>>output_record[i];
    Roll_controller.Evoke_controller(para_record, output_record);

    for(int i=0;i<count;i++) in_info>>para_record[i];
    in_info>>size; output_record.resize(size);
    for(int i=0;i<size;i++)  in_info>>output_record[i];
    Yaw_controller.Evoke_controller(para_record, output_record);

    in_info>>lastbodycentre[0]>>lastbodycentre[1]>>lastbodycentre[2];

    Update_Flap_Kinematics();

    return 0;
}

int FlapPattern :: Record_Flap_Pattern(ofstream &out_info)
{
	if(!out_info.is_open()) {
		cout<<"Unable to open Motion Record File!"<<endl;
		return 1;
	}
	
	out_info<<cyc_counter<<" "<<total_cyc_time<<" "<<cyc_time<<" "
	        <<stime_start<<" "<<stime_end<<endl;
	
	// controller
	out_info<<freq<<" "<<freq_old<<" "<<freq_global<<endl;
	
	out_info<<Angle_Previous_Phi<<" "
	        <<Angle_Current_Phi<<" "
	        <<Angle_Next_Phi<<endl;
	out_info<<Angle_Previous_Theta<<" "
	        <<Angle_Current_Theta<<" "
	        <<Angle_Next_Theta<<endl;
	out_info<<Angle_Previous_Psi<<" "
	        <<Angle_Current_Psi<<" "
	        <<Angle_Next_Psi<<endl;
	out_info<<Angle_Previous_Stroke_Delta<<" "
	        <<Angle_Current_Stroke_Delta<<" "
	        <<Angle_Next_Stroke_Delta<<endl;
	out_info<<Angle_Previous_AoA_Delta<<" "
	        <<Angle_Current_AoA_Delta<<" "
	        <<Angle_Next_AoA_Delta<<endl;
	out_info<<Angle_Previous_Psi_Delta<<" "
	        <<Angle_Current_Psi_Delta<<" "
	        <<Angle_Next_Psi_Delta<<endl;
    
    int count = 6, size; 
    double         para_record[count];
    vector<double> output_record;
    
    X_controller.Record_controller(para_record, output_record);
    for(int i=0;i<count;i++) out_info<<para_record[i]<<" "; out_info<<endl;
    size = (output_record.size() > 50) ? 50 : output_record.size();
    out_info << size << " ";
    for(int i=0;i<size;i++) 
        out_info << output_record[output_record.size() - size + i] << " "; 
    out_info << endl;
    
    Y_controller.Record_controller(para_record, output_record);
    for(int i=0;i<count;i++) out_info<<para_record[i]<<" "; out_info<<endl;
    size = (output_record.size() > 50) ? 50 : output_record.size();
    out_info << size << " ";
    for(int i=0;i<size;i++) 
        out_info << output_record[output_record.size() - size + i] << " "; 
    out_info << endl;
    
    Z_controller.Record_controller(para_record, output_record);
    for(int i=0;i<count;i++) out_info<<para_record[i]<<" "; out_info<<endl;
    size = (output_record.size() > 50) ? 50 : output_record.size();
    out_info << size << " ";
    for(int i=0;i<size;i++) 
        out_info << output_record[output_record.size() - size + i] << " "; 
    out_info << endl;
    
    V_controller.Record_controller(para_record, output_record);
    for(int i=0;i<count;i++) out_info<<para_record[i]<<" "; out_info<<endl;
    size = (output_record.size() > 50) ? 50 : output_record.size();
    out_info << size << " ";
    for(int i=0;i<size;i++) 
        out_info << output_record[output_record.size() - size + i] << " "; 
    out_info << endl;
    
    Orientation_controller.Record_controller(para_record, output_record);
    for(int i=0;i<count;i++) out_info<<para_record[i]<<" "; out_info<<endl;
    size = (output_record.size() > 50) ? 50 : output_record.size();
    out_info << size << " ";
    for(int i=0;i<size;i++) 
        out_info << output_record[output_record.size() - size + i] << " "; 
    out_info << endl;
    
    Roll_controller.Record_controller(para_record, output_record);
    for(int i=0;i<count;i++) out_info<<para_record[i]<<" "; out_info<<endl;
    size = (output_record.size() > 50) ? 50 : output_record.size();
    out_info << size << " ";
    for(int i=0;i<size;i++) 
        out_info << output_record[output_record.size() - size + i] << " "; 
    out_info << endl;
    
    Yaw_controller.Record_controller(para_record, output_record);
    for(int i=0;i<count;i++) out_info<<para_record[i]<<" "; out_info<<endl;
    size = (output_record.size() > 50) ? 50 : output_record.size();
    out_info << size << " ";
    for(int i=0;i<size;i++) 
        out_info << output_record[output_record.size() - size + i] << " "; 
    out_info << endl;
    
	for(int i=0;i<3;i++) out_info << lastbodycentre[i] << " "; 
	out_info << endl;
	
	return 0;
}


int FlapPattern :: Record_Flap_Pattern_History(double mtime,
								  /*for test*/ double wp_a[3], double w1_a[3], double w2_a[3], double freq)
{
	if(!flap_history.is_open()) {
		cout<<"Unable to open Flap History File!"<<endl;
		return 1;
	}
	
	flap_history<<mtime<<" "
	            <<w1_a[0]<<" "<<w1_a[1]<<" "<<w1_a[2]<<" "
	            <<w2_a[0]<<" "<<w2_a[1]<<" "<<w2_a[2]<<" "
	            <<freq<<" "
	            <<Angle_Current_Stroke_Delta<<" "
	            <<Angle_Current_AoA_Delta<<" "
	            <<Angle_Previous_Phi<<" "
	            <<Angle_Previous_Theta<<" "
	            <<Angle_Previous_Psi<<" "
	            <<wp_a[0]<<" "<<Angle_Current_Psi_Delta<<" "<<wp_a[2]<<" "
	            <<endl;
	
	return 0;
}

