#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#ifdef _OPENMP
#	include <omp.h>
#endif

#include "../common/tec360/MASTER.h" /*--Tecplot Head--*/
#include "../common/tec360/TECIO.h"  /*--Tecplot Head--*/
#include "../common/frame_structure.h"
#include "../common/write_result.h"

using namespace std;

#define  N_VARS 7  //  *X, *Y, *Z, *U, *V, *W, *P

extern double *X, *Y, *Z, *U, *V, *W, *P;
extern double Time;
extern int IPoint, JPoint, KPoint, Point_All, Point_Car, Point_Meshless;
extern int *IIndex, *JIndex, *Rigid_Offset;
extern Ref_Frame lab, insect, wingplane_1, wing_1;
extern double bodycentre[3];

	/* 	Tecplot ZoneType
	 	0=ORDERED
		1=FELINESEG  (NumIndices = 2)
		2=FETRIANGLE  (NumIndices = 3)
		3=FEQUADRILATERAL  (NumIndices = 4)
		4=FETETRAHEDRON  (NumIndices = 4)
		5=FEBRICK  (NumIndices = 8)
		6=FEPOLYGON
		7=FEPOLYHEDRON 
	*/

#pragma GCC diagnostic ignored "-Wwrite-strings"  // gets rid of "deprecated conversion from string constant " warning
int Write_Result_PLT_Cartesian(int iter, int freq)
{
	char *filename = new char [64];	
	char Char_Buffer[33];
	strcpy(filename, "../result/Cartesian-");
	sprintf(Char_Buffer, "%d", int(iter/freq));
	strcat(filename, Char_Buffer);
	strcat(filename, ".plt");

	INTEGER4 FileType = 0;
	INTEGER4 Debug = 0;
	INTEGER4 VIsDouble = 0;
	INTEGER4 I = 0; /* Used to track return codes */
	
	if (Debug == 1)
	    cout << '\n' << "******************** Generating Cartesian Results ********************" << endl;
	
	/* Open the file and write the tecplot datafile header information */
	I = TECINI112(	"Cartesian Point", /* TITLE */
					"X Y Z U V W P", /* VARIABLES */
					filename, /* FILENAME */
					".", /* Scratch Directory */
					&FileType,
					&Debug,
					&VIsDouble);
	
	/* Ordered Zone Parameters */
	INTEGER4 ZoneType = 0; 
	INTEGER4 IMax = KPoint;
	INTEGER4 JMax = JPoint;
	INTEGER4 KMax = IPoint;	
	INTEGER4 ICellMax = 0;
	INTEGER4 JCellMax = 0;
	INTEGER4 KCellMax = 0;
	double SolTime = Time;
	INTEGER4 StrandID = 100;
	INTEGER4 ParentZn = 0;
	INTEGER4 IsBlock = 1;  /* 1 - Block */
	INTEGER4 NFConns = 0;
	INTEGER4 FNMode = 0;
	INTEGER4 TotalNumFaceNodes = 1;
	INTEGER4 TotalNumBndryFaces = 1;
	INTEGER4 TotalNumBndryConnections = 1;
	INTEGER4 *PassiveVarArray = NULL;
	INTEGER4 *ValueLocArray = NULL;
	INTEGER4 *VarShareArray = NULL;
	INTEGER4 ShrConn = 0;
	
	/* Ordered Zone */
	I = TECZNE112(	"100",  /* ZoneTitle */
					&ZoneType,
					&IMax,
					&JMax,
					&KMax,
					&ICellMax,
					&JCellMax,
					&KCellMax,
					&SolTime, /* SolutionTime */
					&StrandID,
					&ParentZn,
					&IsBlock,
					&NFConns,
					&FNMode,
					&TotalNumFaceNodes,
					&TotalNumBndryFaces,
					&TotalNumBndryConnections,
					PassiveVarArray,
					ValueLocArray,
					VarShareArray,
					&ShrConn);
	
	INTEGER4 DIsDouble = 1;
	INTEGER4 NumPts = Point_Car;
	
    double *tmp2 = new double [NumPts];
#   pragma omp parallel for
    for(int j=0;j<NumPts;j++)
        tmp2[j] = X[j] + bodycentre[0] - 8.0;
    I = TECDAT112(&NumPts,tmp2,&DIsDouble);
#   pragma omp parallel for
    for(int j=0;j<NumPts;j++)
        tmp2[j] = Y[j] + bodycentre[1] - 8.0;
    I = TECDAT112(&NumPts,tmp2,&DIsDouble);
#   pragma omp parallel for
    for(int j=0;j<NumPts;j++)
        tmp2[j] = Z[j] + bodycentre[2] - 8.0;
    I = TECDAT112(&NumPts,tmp2,&DIsDouble);
    delete[] tmp2; tmp2 = NULL;
	
//	I = TECDAT112(&NumPts,&X[0],&DIsDouble);
//	I = TECDAT112(&NumPts,&Y[0],&DIsDouble);
//	I = TECDAT112(&NumPts,&Z[0],&DIsDouble);
	I = TECDAT112(&NumPts,&U[0],&DIsDouble);
	I = TECDAT112(&NumPts,&V[0],&DIsDouble);
	I = TECDAT112(&NumPts,&W[0],&DIsDouble);
	I = TECDAT112(&NumPts,&P[0],&DIsDouble);
	
	I = TECEND112();
	
	delete filename;
	
	if (Debug == 1)
	    cout << "**********************************************************************" << '\n' << endl;
	return 0;
}
#pragma GCC diagnostic warning "-Wwrite-strings"

#pragma GCC diagnostic ignored "-Wwrite-strings"  // gets rid of "deprecated conversion from string constant " warning
int Write_Result_PLT_Meshless(int iter, int freq)
{
	char *filename = new char [64];	
	char Char_Buffer[33];
	strcpy(filename, "../result/Meshless-");
	sprintf(Char_Buffer, "%d", int(iter/freq));
	strcat(filename, Char_Buffer);
	strcat(filename, ".plt");

	INTEGER4 FileType = 0;
	INTEGER4 Debug = 0;
	INTEGER4 VIsDouble = 0;
	INTEGER4 I = 0; /* Used to track return codes */
	
	if (Debug == 1)
	    cout << '\n' << "******************** Generating Meshless Results *********************" << endl;
    
	/* Open the file and write the tecplot datafile header information */
	I = TECINI112(	"Meshless Point", /* TITLE */
					"X Y Z U V W P", /* VARIABLES */
					filename, /* FILENAME */
					".", /* Scratch Directory */
					&FileType,
					&Debug,
					&VIsDouble);
	
	/* Zone Parameters */
	INTEGER4 ZoneType, NumIndices;
	INTEGER4 NumNodes, NumElems, NumFaces = 0;
	INTEGER4 ICellMax = 0, JCellMax = 0, KCellMax = 0;
	double SolTime = Time;
	INTEGER4 StrandID = 0;
	INTEGER4 ParentZn = 0;
	INTEGER4 IsBlock = 1;  /* 1 - Block */
	INTEGER4 NFConns = 0;
	INTEGER4 FNMode = 0;
	INTEGER4 TotalNumFaceNodes = 1;
	INTEGER4 TotalNumBndryFaces = 1;
	INTEGER4 TotalNumBndryConnections = 1;
	INTEGER4 PassiveVarArray[N_VARS];
	INTEGER4 ValueLocArray[N_VARS];
	INTEGER4 VarShareArray[N_VARS];
	INTEGER4 ShrConn = 0;
	
	/* Data Parameters*/
	INTEGER4 DIsDouble = 1;
	
	/*Meshless Points*/
	ZoneType = 4; NumIndices = 4;
	for(int i=0;i<lab.obj_number;i++) {
		NumNodes = lab.rigid_body[i]->POINT_NUMBER;
		NumElems = lab.rigid_body[i]->TETRAHEDRON_NUMBER;
		StrandID = i+1;
		
		sprintf(Char_Buffer, "%d", StrandID);
		I = TECZNE112(	Char_Buffer,  /* ZoneTitle */
						&ZoneType,
						&NumNodes,
						&NumElems,
						&NumFaces,
						&ICellMax,
						&JCellMax,
						&KCellMax,
						&SolTime, /* SolutionTime */
						&StrandID,
						&ParentZn,
						&IsBlock,
						&NFConns,
						&FNMode,
						&TotalNumFaceNodes,
						&TotalNumBndryFaces,
						&TotalNumBndryConnections,
						NULL, // PassiveVarArray
						NULL, // ValueLocArray
						NULL, // VarShareArray
						&ShrConn);
		
		double *tmp2 = new double [NumNodes];
#		pragma omp parallel for
		for(int j=0;j<NumNodes;j++)
			tmp2[j] = X[Point_Car+Rigid_Offset[i]+j] + bodycentre[0] - 8.0;
		I = TECDAT112(&NumNodes,tmp2,&DIsDouble);
#		pragma omp parallel for
		for(int j=0;j<NumNodes;j++)
			tmp2[j] = Y[Point_Car+Rigid_Offset[i]+j] + bodycentre[1] - 8.0;
		I = TECDAT112(&NumNodes,tmp2,&DIsDouble);
#		pragma omp parallel for
		for(int j=0;j<NumNodes;j++)
			tmp2[j] = Z[Point_Car+Rigid_Offset[i]+j] + bodycentre[2] - 8.0;
		I = TECDAT112(&NumNodes,tmp2,&DIsDouble);
		delete[] tmp2; tmp2 = NULL;
		
//		I = TECDAT112(&NumNodes,&X[Point_Car+Rigid_Offset[i]],&DIsDouble);
//		I = TECDAT112(&NumNodes,&Y[Point_Car+Rigid_Offset[i]],&DIsDouble);
//		I = TECDAT112(&NumNodes,&Z[Point_Car+Rigid_Offset[i]],&DIsDouble);
		I = TECDAT112(&NumNodes,&U[Point_Car+Rigid_Offset[i]],&DIsDouble);
		I = TECDAT112(&NumNodes,&V[Point_Car+Rigid_Offset[i]],&DIsDouble);
		I = TECDAT112(&NumNodes,&W[Point_Car+Rigid_Offset[i]],&DIsDouble);
		I = TECDAT112(&NumNodes,&P[Point_Car+Rigid_Offset[i]],&DIsDouble);
		
		int *tmp = new int [NumElems*NumIndices];
#		pragma omp parallel for
		for(int j=0;j<NumElems;j++)
		for(int k=0;k<NumIndices;k++) 
			tmp[k+j*NumIndices]=lab.rigid_body[i]->TETRAHEDRON[k][j];
		I = TECNOD112(tmp);
		delete[] tmp; tmp = NULL;
	}
	
	/* Inner Surface */
	ZoneType = 2; NumIndices = 3;
	for(int i=0;i<lab.obj_number;i++) {
		NumNodes = lab.rigid_body[i]->POINT_NUMBER;
		NumElems = lab.rigid_body[i]->TRIANGLE_NUMBER;
		StrandID = lab.obj_number+i+1;
		for(int j=0;j<N_VARS;j++) VarShareArray[j] = i+1;
		
		sprintf(Char_Buffer, "%d", StrandID);
		I = TECZNE112(	Char_Buffer,  // ZoneTitle 
						&ZoneType,
						&NumNodes,
						&NumElems,
						&NumFaces,
						&ICellMax,
						&JCellMax,
						&KCellMax,
						&SolTime, // SolutionTime 
						&StrandID,
						&ParentZn,
						&IsBlock,
						&NFConns,
						&FNMode,
						&TotalNumFaceNodes,
						&TotalNumBndryFaces,
						&TotalNumBndryConnections,
						NULL, // PassiveVarArray
						NULL, // ValueLocArray
						VarShareArray,
						&ShrConn);
		
		int *tmp = new int [NumElems*NumIndices];
#		pragma omp parallel for
		for(int j=0;j<NumElems;j++)
		for(int k=0;k<NumIndices;k++) 
			tmp[k+j*NumIndices]=lab.rigid_body[i]->TRIANGLE[k][j];
		I = TECNOD112(tmp);
		delete[] tmp; tmp = NULL;
	}
	
	I = TECEND112();
	delete filename;
	
    if (Debug == 1)
	    cout << "**********************************************************************" << '\n' << endl;
	return 0;
}
#pragma GCC diagnostic warning "-Wwrite-strings"

#pragma GCC diagnostic ignored "-Wwrite-strings"  // gets rid of "deprecated conversion from string constant " warning
int Write_Result_PLT_RealSurface(int iter, int freq)
{
	char *filename = new char [64];	
	char Char_Buffer[33];
	strcpy(filename, "../result/RealSurface-");
	sprintf(Char_Buffer, "%d", int(iter/freq));
	strcat(filename, Char_Buffer);
	strcat(filename, ".plt");

	INTEGER4 FileType = 0;
	INTEGER4 Debug = 0;
	INTEGER4 VIsDouble = 1;
	INTEGER4 I = 0; /* Used to track return codes */
	
	if (Debug == 1)
	    cout << "******************** Generating Real Surface Results *********************" << '\n' << endl;
	
	/* Open the file and write the tecplot datafile header information */
	I = TECINI112(	"Real Surface Points", /* TITLE */
					"X Y Z U V W P", /* VARIABLES */
					filename, /* FILENAME */
					".", /* Scratch Directory */
					&FileType,
					&Debug,
					&VIsDouble);
	
	/* Zone Parameters */
	INTEGER4 ZoneType, NumIndices;
	INTEGER4 NumNodes, NumElems, NumFaces = 0;
	INTEGER4 ICellMax = 0, JCellMax = 0, KCellMax = 0;
	double SolTime = Time;
	INTEGER4 StrandID = 0;
	INTEGER4 ParentZn = 0;
	INTEGER4 IsBlock = 1;  /* 1 - Block */
	INTEGER4 NFConns = 0;
	INTEGER4 FNMode = 0;
	INTEGER4 TotalNumFaceNodes = 1;
	INTEGER4 TotalNumBndryFaces = 1;
	INTEGER4 TotalNumBndryConnections = 1;
	INTEGER4 PassiveVarArray[N_VARS];
	INTEGER4 ValueLocArray[N_VARS];
	INTEGER4 VarShareArray[N_VARS];
	INTEGER4 ShrConn = 0;
	
	/* Data Parameters*/
	INTEGER4 DIsDouble = 1;
	
	/* Inner Surface */
	int *tmp1; double *tmp2;
	ZoneType = 2; NumIndices = 3;
	for(int i=0;i<lab.obj_number;i++) {
		NumNodes = lab.rigid_body[i]->POINT_NUMBER;
		NumElems = lab.rigid_body[i]->TRIANGLE_NUMBER;
		StrandID = lab.obj_number+i+1;
		
		sprintf(Char_Buffer, "%d", StrandID);
		I = TECZNE112(	Char_Buffer,  // ZoneTitle 
						&ZoneType,
						&NumNodes,
						&NumElems,
						&NumFaces,
						&ICellMax,
						&JCellMax,
						&KCellMax,
						&SolTime, // SolutionTime 
						&StrandID,
						&ParentZn,
						&IsBlock,
						&NFConns,
						&FNMode,
						&TotalNumFaceNodes,
						&TotalNumBndryFaces,
						&TotalNumBndryConnections,
						NULL, // PassiveVarArray
						NULL, // ValueLocArray
						NULL, // VarShareArray
						&ShrConn);
		
		tmp2 = new double [NumNodes];
#		pragma omp parallel for
		for(int j=0;j<NumNodes;j++)
			tmp2[j] = X[Point_Car+Rigid_Offset[i]+j] + bodycentre[0] - 8.0;
		I = TECDAT112(&NumNodes,tmp2,&DIsDouble);
#		pragma omp parallel for
		for(int j=0;j<NumNodes;j++)
			tmp2[j] = Y[Point_Car+Rigid_Offset[i]+j] + bodycentre[1] - 8.0;
		I = TECDAT112(&NumNodes,tmp2,&DIsDouble);
#		pragma omp parallel for
		for(int j=0;j<NumNodes;j++)
			tmp2[j] = Z[Point_Car+Rigid_Offset[i]+j] + bodycentre[2] - 8.0;
		I = TECDAT112(&NumNodes,tmp2,&DIsDouble);
		delete[] tmp2; tmp2 = NULL;
		I = TECDAT112(&NumNodes,&U[Point_Car+Rigid_Offset[i]],&DIsDouble);
		I = TECDAT112(&NumNodes,&V[Point_Car+Rigid_Offset[i]],&DIsDouble);
		I = TECDAT112(&NumNodes,&W[Point_Car+Rigid_Offset[i]],&DIsDouble);
		I = TECDAT112(&NumNodes,&P[Point_Car+Rigid_Offset[i]],&DIsDouble);
		
		tmp1 = new int [NumElems*NumIndices];
#		pragma omp parallel for
		for(int j=0;j<NumElems;j++)
		for(int k=0;k<NumIndices;k++) 
			tmp1[k+j*NumIndices]=lab.rigid_body[i]->TRIANGLE[k][j];
		I = TECNOD112(tmp1);
		delete[] tmp1; tmp1 = NULL;
	}
	
	I = TECEND112();
	delete filename;
	
    if (Debug == 1)
	    cout << "**********************************************************************" << '\n' << endl;
    
	return 0;
}

#pragma GCC diagnostic warning "-Wwrite-strings"
void Write_Result_PLT(int iter)
{
	int freq_cartesian=50;
	// if(iter%freq_cartesian==0 || iter==15001) Write_Result_PLT_Cartesian(iter, freq_cartesian);
	
	int freq_meshless=50;
	if(iter%freq_meshless==0 || iter==1) Write_Result_PLT_Meshless(iter, freq_meshless);
//	if(iter%freq_meshless==0) Write_Result_PLT_RealSurface(iter, freq_meshless);
}

#pragma GCC diagnostic warning "-Wwrite-strings"
void Write_Result_DIV(int iter)
{
	int freq_cartesian=1;
	// if(iter%freq_cartesian==0 || iter==15001) 
	Write_Result_PLT_Cartesian(iter, freq_cartesian);
	
	int freq_meshless=1;
	// if(iter%freq_meshless==0 || iter==1) 
	Write_Result_PLT_Meshless(iter, freq_meshless);
//	if(iter%freq_meshless==0) Write_Result_PLT_RealSurface(iter, freq_meshless);
}


void Write_Result_DAT(int iter)
{
	int frequency=50;
	if(iter%frequency==0) {
		char *filename = new char [30];	
		char Char_Buffer[30];
		strcpy(filename, "../result/Meshless-");
		sprintf(Char_Buffer, "%d", int(iter/frequency));
		strcat(filename, Char_Buffer);
		strcat(filename, ".dat");

		ofstream out;
		out.open(filename);
		out<<"TITLE= \"Meshless Point\""<<endl;
		out<<"VARIABLES = \"X\", \"Y\", \"Z\", \"U\", \"V\", \"W\", \"P\""<<endl;
		for(int i=0;i<lab.obj_number;i++) {
			out<<"ZONE T="<<"\""<<i<<"\""<<", "<<"SOLUTIONTIME="<<Time<<", "
			   <<"N="<<lab.rigid_body[i]->POINT_NUMBER<<", "
			   <<"E="<<lab.rigid_body[i]->TETRAHEDRON_NUMBER<<", "
			   <<"DATAPACKING=POINT, ZONETYPE=FETETRAHEDRON"<<endl;
			
			for(int j=0;j<lab.rigid_body[i]->POINT_NUMBER;j++) {
				out<<X[Point_Car+j+Rigid_Offset[i]]<<" "
				   <<Y[Point_Car+j+Rigid_Offset[i]]<<" "
				   <<Z[Point_Car+j+Rigid_Offset[i]]<<" "
				   <<U[Point_Car+j+Rigid_Offset[i]]<<" "
				   <<V[Point_Car+j+Rigid_Offset[i]]<<" "
				   <<W[Point_Car+j+Rigid_Offset[i]]<<" "
				   <<P[Point_Car+j+Rigid_Offset[i]]<<endl;
			}
			
			for(int j=0;j<lab.rigid_body[i]->TETRAHEDRON_NUMBER;j++) {
				out<<lab.rigid_body[i]->TETRAHEDRON[0][j]<<" "
				   <<lab.rigid_body[i]->TETRAHEDRON[1][j]<<" "
				   <<lab.rigid_body[i]->TETRAHEDRON[2][j]<<" "
				   <<lab.rigid_body[i]->TETRAHEDRON[3][j]<<endl;
			}
		}
		out.close();
		delete filename;
		
		if(iter%500==0) {
		
		char *filename2 = new char [30];	
		char Char_Buffer2[30];
		strcpy(filename2, "../result/Cartesian-");
		sprintf(Char_Buffer2, "%d", int(iter/frequency));
		strcat(filename2, Char_Buffer2);
		strcat(filename2, ".dat");
		
		ofstream out2;
		out2.open(filename2);
		out2<<"TITLE= \"Cartesian Point\""<<endl;
		out2<<"VARIABLES = \"X\", \"Y\", \"Z\", \"U\", \"V\", \"W\", \"P\""<<endl;
		out2<<"Zone T="<<"\""<<4<<"\""<<", "<<"SOLUTIONTIME="<<Time<<", "
		    <<", I="<<IPoint<<", J="<<JPoint<<", K="<<KPoint<<", F=POINT"<<endl;
		int ijk;
		for(int k=0;k<=KPoint-1;k++) {	
			for(int j=0;j<=JPoint-1;j++) {
				for(int i=0;i<=IPoint-1;i++) {
					ijk=IIndex[i]+JIndex[j]+k;
					out2<<X[ijk]<<" "<<Y[ijk]<<" "<<Z[ijk]<<" "
						<<U[ijk]<<" "<<V[ijk]<<" "<<W[ijk]<<" "<<P[ijk]<<endl;
				}
			}
		}
		out2.close();
		delete filename2;
		
		}

		char *filename3 = new char [30];	
		char Char_Buffer3[30];
		strcpy(filename3, "../result/Surface-");
		sprintf(Char_Buffer3, "%d", int(iter/frequency));
		strcat(filename3, Char_Buffer3);
		strcat(filename3, ".dat");
		ofstream out3; 
		out3.open(filename3);
		out3<<"TITLE= \"Surface Point\""<<endl;
		out3<<"VARIABLES = \"X\", \"Y\", \"Z\", \"U\", \"V\", \"W\", \"P\""<<endl;
		for(int i=0;i<lab.obj_number;i++) {
			out3<<"ZONE T="<<"\""<<i<<"\""<<", "<<"SOLUTIONTIME="<<Time<<", "
				<<"N="<<lab.rigid_body[i]->POINT_NUMBER<<", "
				<<"E="<<lab.rigid_body[i]->TRIANGLE_NUMBER<<", "
				<<"DATAPACKING=POINT, ZONETYPE=FETRIANGLE"<<endl;
			for(int j=0;j<lab.rigid_body[i]->POINT_NUMBER;j++) {
				out3<<X[Point_Car+j+Rigid_Offset[i]]<<" "
					<<Y[Point_Car+j+Rigid_Offset[i]]<<" "
					<<Z[Point_Car+j+Rigid_Offset[i]]<<" "
					<<U[Point_Car+j+Rigid_Offset[i]]<<" "
					<<V[Point_Car+j+Rigid_Offset[i]]<<" "
					<<W[Point_Car+j+Rigid_Offset[i]]<<" "
					<<P[Point_Car+j+Rigid_Offset[i]]<<endl;
				//out3<<showindex[j+Rigid_Offset[i]]<<endl;
			}
			for(int k=0;k<lab.rigid_body[i]->TRIANGLE_NUMBER;k++) {
				out3<<lab.rigid_body[i]->TRIANGLE[0][k]<<" "
					<<lab.rigid_body[i]->TRIANGLE[1][k]<<" "
					<<lab.rigid_body[i]->TRIANGLE[2][k]<<endl;
			}

			
		}
		out3.close();
		delete filename3;
		
		
		char *filename4 = new char [30];	
		char Char_Buffer4[30];
		strcpy(filename4, "../result/RealSurface-");
		sprintf(Char_Buffer4, "%d", int(iter/frequency));
		strcat(filename4, Char_Buffer4);
		strcat(filename4, ".dat");
		ofstream out4; 
		out4.open(filename4);
		out4<<"TITLE= \"Surface Point\""<<endl;
		out4<<"VARIABLES = \"X\", \"Y\", \"Z\", \"U\", \"V\", \"W\", \"P\""<<endl;
		for(int i=0;i<lab.obj_number;i++) {
			out4<<"ZONE T="<<"\""<<i<<"\""<<", "<<"SOLUTIONTIME="<<Time<<", "
				<<"N="<<lab.rigid_body[i]->POINT_NUMBER<<", "
				<<"E="<<lab.rigid_body[i]->TRIANGLE_NUMBER<<", "
				<<"DATAPACKING=POINT, ZONETYPE=FETRIANGLE"<<endl;
			for(int j=0;j<lab.rigid_body[i]->POINT_NUMBER;j++) {
				out4<<X[Point_Car+j+Rigid_Offset[i]]+bodycentre[0]-8.0<<" "
					<<Y[Point_Car+j+Rigid_Offset[i]]+bodycentre[1]-8.0<<" "
					<<Z[Point_Car+j+Rigid_Offset[i]]+bodycentre[2]-8.0<<" "
					<<U[Point_Car+j+Rigid_Offset[i]]<<" "
					<<V[Point_Car+j+Rigid_Offset[i]]<<" "
					<<W[Point_Car+j+Rigid_Offset[i]]<<" "
					<<P[Point_Car+j+Rigid_Offset[i]]<<endl;
			}
			for(int k=0;k<lab.rigid_body[i]->TRIANGLE_NUMBER;k++) {
				out4<<lab.rigid_body[i]->TRIANGLE[0][k]<<" "
					<<lab.rigid_body[i]->TRIANGLE[1][k]<<" "
					<<lab.rigid_body[i]->TRIANGLE[2][k]<<endl;
			}
			
		}
		out4.close();
		delete filename4;
	}
}


void Frame_tranformation(int it)
{
    int frequency=10000000;
    if(it%frequency==0)
    {
	double *tempX1,*tempY1,*tempZ1,*tempU1,*tempV1,*tempW1,*tempX2,*tempY2,*tempZ2,*tempU2,*tempV2,*tempW2;
	tempX1=new double [Point_All];
	tempY1=new double [Point_All];
	tempZ1=new double [Point_All];
	tempU1=new double [Point_All];
	tempV1=new double [Point_All];
	tempW1=new double [Point_All];
	tempX2=new double [Point_All];
	tempY2=new double [Point_All];
	tempZ2=new double [Point_All];
	tempU2=new double [Point_All];
	tempV2=new double [Point_All];
	tempW2=new double [Point_All];
	
	inverse_change_ref_frame(&lab,X,Y,Z,U,V,W,&insect,tempX1,tempY1,tempZ1,tempU1,tempV1,tempW1);
	inverse_change_ref_frame(&insect,tempX1,tempY1,tempZ1,tempU1,tempV1,tempW1,&wingplane_1,tempX2,tempY2,tempZ2,tempU2,tempV2,tempW2);
	inverse_change_ref_frame(&wingplane_1,tempX2,tempY2,tempZ2,tempU2,tempV2,tempW2,&wing_1,tempX1,tempY1,tempZ1,tempU1,tempV1,tempW1);
	
	char *filename2 = new char [64];
	char Char_Buffer2[32];
	strcpy(filename2, "../result/Transform-Cartesian-");
	sprintf(Char_Buffer2, "%d", int(it/frequency));
	strcat(filename2, Char_Buffer2);
	strcat(filename2, ".dat");
		
	ofstream out2;
	out2.open(filename2);
	out2<<"TITLE= \"Cartesian Point\""<<endl;
	out2<<"VARIABLES = \"X\", \"Y\", \"Z\", \"U\", \"V\", \"W\", \"P\""<<endl;
	out2<<"Zone T="<<"\""<<4<<"\""<<" I="<<IPoint<<", J="<<JPoint<<", K="<<KPoint<<", F=POINT"<<endl;
	int ijk;
	for(int k=0;k<=KPoint-1;k++)
	{	
		for(int j=0;j<=JPoint-1;j++)
		{
			for(int i=0;i<=IPoint-1;i++)
			{
				ijk=IIndex[i]+JIndex[j]+k;
				out2<<tempX1[ijk]<<" "<<tempY1[ijk]<<" "<<tempZ1[ijk]<<" "<<tempU1[ijk]<<" "<<tempV1[ijk]<<" "<<tempW1[ijk]<<" "<<P[ijk]<<endl;
			}
		}
	}
	out2.close();
	delete filename2;
	
	
	
	char *filename = new char [64];	
	char Char_Buffer[32];
	strcpy(filename, "../result/Tranform-Meshless-");
	sprintf(Char_Buffer, "%d", int(it/frequency));
	strcat(filename, Char_Buffer);
	strcat(filename, ".dat");

	ofstream out;
	out.open(filename);
	out<<"TITLE= \"Meshless Point\""<<endl;
	out<<"VARIABLES = \"X\", \"Y\", \"Z\", \"U\", \"V\", \"W\", \"P\""<<endl;
	for(int i=0;i<lab.obj_number;i++)
	{
		out<<"ZONE T="<<"\""<<i<<"\""<<" "<<"N="<<lab.rigid_body[i]->POINT_NUMBER<<", "<<"E="<<lab.rigid_body[i]->TETRAHEDRON_NUMBER<<", "<<"DATAPACKING=POINT, ZONETYPE=FETETRAHEDRON"<<endl;
		for(int j=0;j<lab.rigid_body[i]->POINT_NUMBER;j++)
		{
			out<<tempX1[Point_Car+j+Rigid_Offset[i]]<<" "<<tempY1[Point_Car+j+Rigid_Offset[i]]<<" "<<tempZ1[Point_Car+j+Rigid_Offset[i]]<<" ";
			out<<tempU1[Point_Car+j+Rigid_Offset[i]]<<" "<<tempV1[Point_Car+j+Rigid_Offset[i]]<<" "<<tempW1[Point_Car+j+Rigid_Offset[i]]<<" "<<P[Point_Car+j+Rigid_Offset[i]]<<endl;
		}
			
		for(int j=0;j<lab.rigid_body[i]->TETRAHEDRON_NUMBER;j++)
		{
			out<<lab.rigid_body[i]->TETRAHEDRON[0][j]<<" "<<lab.rigid_body[i]->TETRAHEDRON[1][j]<<" "<<lab.rigid_body[i]->TETRAHEDRON[2][j]<<" "<<lab.rigid_body[i]->TETRAHEDRON[3][j]<<endl;
		}
	}
	out.close();
	delete filename;
	
	
	
	
	char *filename3 = new char [64];	
	char Char_Buffer3[32];
	strcpy(filename3, "../result/Transform-Surface-");
	sprintf(Char_Buffer3, "%d", int(it/frequency));
	strcat(filename3, Char_Buffer3);
	strcat(filename3, ".dat");
	ofstream out3; 
	out3.open(filename3);
	out3<<"TITLE= \"Surface Point\""<<endl;
	out3<<"VARIABLES = \"X\", \"Y\", \"Z\", \"U\", \"V\", \"W\", \"P\""<<endl;
	for(int i=0;i<lab.obj_number;i++)
	{
		out3<<"ZONE T="<<"\""<<i<<"\""<<" "<<"N="<<lab.rigid_body[i]->POINT_NUMBER<<", "<<"E="<<lab.rigid_body[i]->TRIANGLE_NUMBER<<", "<<"DATAPACKING=POINT, ZONETYPE=FETRIANGLE"<<endl;
		for(int j=0;j<lab.rigid_body[i]->POINT_NUMBER;j++)
		{
			out3<<tempX1[Point_Car+j+Rigid_Offset[i]]<<" "<<tempY1[Point_Car+j+Rigid_Offset[i]]<<" "<<tempZ1[Point_Car+j+Rigid_Offset[i]]<<" ";
			out3<<tempU1[Point_Car+j+Rigid_Offset[i]]<<" "<<tempV1[Point_Car+j+Rigid_Offset[i]]<<" "<<tempW1[Point_Car+j+Rigid_Offset[i]]<<" "<<P[Point_Car+j+Rigid_Offset[i]]<<endl;
			//out3<<U[Point_Car+j+Rigid_Offset[i]]<<" "<<V[Point_Car+j+Rigid_Offset[i]]<<" "<<W[Point_Car+j+Rigid_Offset[i]]<<" "<<P[Point_Car+j+Rigid_Offset[i]]<<" "<<showindex[j+Rigid_Offset[i]]<<endl;
		}
		for(int k=0;k<lab.rigid_body[i]->TRIANGLE_NUMBER;k++)
		{
			out3<<lab.rigid_body[i]->TRIANGLE[0][k]<<" "<<lab.rigid_body[i]->TRIANGLE[1][k]<<" "<<lab.rigid_body[i]->TRIANGLE[2][k]<<endl;
		}
	}
	out3.close();
	delete filename3;
	
	
	
	delete[] tempX1;
	delete[] tempY1;
	delete[] tempZ1;
	delete[] tempU1;
	delete[] tempV1;
	delete[] tempW1;
	delete[] tempX2;
	delete[] tempY2;
	delete[] tempZ2;
	delete[] tempU2;
	delete[] tempV2;
	delete[] tempW2;
    }
}

