#ifndef MESH_TYPE_H
#define MESH_TYPE_H

#define NF_MAX 4

#define NB 40
//#define Order 19
#define OrderL 9

// #define UPWIND

namespace GType
{
    typedef char type;
    
    const type INACTIVE     = 0;        //  Nodes temporarily do not take part in computation, results from moving/stationary nodes overlap
    const type _7POINT      = 1;        //  Central Difference treated
    const type OUTFLOW      = 2;        //  Outflow boundaries
    const type _GFDSVD      = 3;        //  Generalized difference treated
    const type NEWBIE       = 4;        //  Nodes just escape from inactive state, waiting for value-interpolated before entering computation
    const type WALL         = 5;        //  Moving surfaces, such as surfaces defining moving bodies
    const type MUTE         = 6;        //  Nodes NEVER take part in computation, such as edge nodes (unreachable by Cartesian schemes)
    
//    const type LAYER_2      = 7;        //  Outflow boundaries
    
    const type MOBILIZED    = _GFDSVD;	//	Nodes switching scheme from _7point to _GfdSvd
    const type DEFAULT      = INACTIVE;
    
    const type CAND_3       = -3;
    const type CAND_4       = -4;
    
    const type CAND_0       = -10;
    
    const type OVLP_3       = -13;
    const type OVLP_4       = -14;
    
    const type RESTART      = 10;
    const type REMARKED     = 127;
    const type RESERVED     = -127;
}

struct MeshlessMember
{    
	int Meshless_Ind;
	int Nb_Points[NB];
//	int Nb_Points_Old[NB];
	double Csvd[OrderL][NB];
//	double Csvd_Old[Order][NB];
};

struct CoordMember
{
    unsigned int idx;
    unsigned int offset[3];
    
    double pos;
    double coef[3][3];
    
#ifdef  UPWIND
    unsigned int offset_upwind[2][3];
    double coef_upwind[2][3];
#endif
};

struct RectBox
{
    unsigned int idx[3]; // index of start point
    unsigned int dim[3]; // dimension of each edge
    double       pos[3]; // position of start point
    double       len[3]; // length of each edge
    double       delta;
};

#endif /*MESH_TYPE_H*/
