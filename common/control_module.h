#ifndef CONTROL_MODULE_H
#define CONTROL_MODULE_H

#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

class PID_Control_Module
{
  public:
    const char*      name;
	double           set;
	double           Kp,Ki,Kd,epsilon,Init;
	double			 Max, Min, Max_Change, Delta;
	vector<double>   record;
	vector<double>   past_cycle_record;
	
	PID_Control_Module() { Max_Change=-1; }
	
	double Activate_controller(double input, double dt, bool is_avg = false) 
	{
		double output;
	
		cout<<"----------------- "<<name<<" is activated! -----------------"<<endl;
	  
		process=input;
	
		if(record.size()<1)
			pre_error=0;
		else
			pre_error=error;
	
		error=set-process;
		Proportional=error;
		if(fabs(error)<epsilon)
		{
			Integral=Integral+error*dt;
		}
		Derivative=(error-pre_error)/dt;
		
	    output=Kp*Proportional+Ki*Integral+Kd*Derivative+Init;
	    
	    if (is_avg)
	    {
//            cout<<"*************Average activated in "<<name<<" is activated!************"<<endl;
            Average_record(10);
	    }
	    
		output = (output>Max) ? Max:output;
		output = (output<Min) ? Min:output;
	
		if(Max_Change>0) {
			if(record.size()<1) {
				output = (output>(Init + Max_Change)) ? (Init + Max_Change):output;
				output = (output<(Init - Max_Change)) ? (Init - Max_Change):output;
			}
			else {
				int tmp_idx = record.size()-1;
				output = (output>(record[tmp_idx]+Max_Change)) ? (record[tmp_idx] + Max_Change):output;
				output = (output<(record[tmp_idx]-Max_Change)) ? (record[tmp_idx] - Max_Change):output;
			}
		}
	
		record.push_back(output);
		return output;
	}
	
	void Sum_past_cycle(double addone, bool isnew)
	{
		if (isnew)
		{
			past_cycle_record.resize(0);
			past_cycle_record.push_back(addone);
		}
		else
		{past_cycle_record.push_back(addone);}
	}

	
	void Record_controller(double protected_var[6], vector<double> &output_history)
	{
		protected_var[0] = Proportional;
		protected_var[1] = Integral;
		protected_var[2] = Derivative;
	
		protected_var[3] = error;
		protected_var[4] = pre_error;
		protected_var[5] = process;
	
//		if(record.size()<1)
//			protected_var[6] = Init;
//		else
//			protected_var[6] = record[record.size()-1];
		output_history = record;
	}
	
	void Evoke_controller(double protected_var[6], vector<double> &output_history) 
	{
		Proportional 	= protected_var[0];
		Integral 		= protected_var[1];
		Derivative 		= protected_var[2];
	
		error 			= protected_var[3];
		pre_error 		= protected_var[4];
		process 		= protected_var[5];
	
//		record.push_back(protected_var[6]);
		record = output_history;
	}
	
  protected:       
	double           Proportional,Integral,Derivative;
	double 			 error,pre_error,process;
	
	void Average_record(int length = 10)
	{
        double sum=0;
        double Mean;
        if (record.size()<length)
        {
            for (int i=0;i<record.size();i++)
            {
                sum += record[i];
            }
            Mean=sum/record.size();
        }
        else
        {
            for (int i=0;i<length;i++)
            {
                sum=sum+record[record.size()-1-i];
            }
            Mean=sum/length;
        }
        Max = Mean + Delta;
        Min = Mean - Delta;
    }
};

#endif /* CONTROL_MODULE_H */
