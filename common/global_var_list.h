#ifndef __GLOBAL_VAR_LIST_H__
#define __GLOBAL_VAR_LIST_H__

#include <vector>

#include "mesh_type.h"

// host pointers
extern int Point_Car, Point_Meshless, Point_All;
extern int IPoint, JPoint, KPoint;
extern int Total_Body_Nodes, Total_Outer_Nodes;

extern int *IIndex, *JIndex, *BNI, *ONI;

extern CoordMember* Cord[3];

extern std::vector<MeshlessMember> C2M_List;
extern MeshlessMember* Cld_List;

extern GType::type *GType2, *GType1, *GTypeT;

extern double *P, *U, *V, *W;
extern double *So, *So_U, *So_V, *So_W; // source contribution from last time step

extern double Car_Ale[3], Car_ACC[3];
extern double *U_Ale, *V_Ale, *W_Ale;
extern double *ACCX_Ale, *ACCY_Ale, *ACCZ_Ale;

extern double *X, *Y, *Z;
extern double *X_Meshless, *Y_Meshless, *Z_Meshless;
extern double *MNx, *MNy, *MNz;

extern double Re, Dt, Deltx, Res[NF_MAX];
extern int    IP;

// device pointers
extern int  *IIndex_dev, *JIndex_dev, *BNI_dev, *ONI_dev;

extern CoordMember *Cordx_dev, *Cordy_dev, *Cordz_dev;

extern MeshlessMember *C2M_List_dev;
extern MeshlessMember *Cld_List_dev;

extern GType::type *GType2_dev, *GType1_dev, *GTypeT_dev;

extern double *P_dev, *U_dev, *V_dev, *W_dev;
extern double *So_dev, *So_U_dev, *So_V_dev, *So_W_dev;
//extern double *Src_U_dev, *Src_V_dev, *Src_W_dev;
extern double *TMP1_dev, *TMP2_dev, *TMP3_dev, *TMP4_dev;
//extern double *Ustar_dev, *Vstar_dev, *Wstar_dev;

extern double *U_Ale_dev, *V_Ale_dev, *W_Ale_dev;
extern double *ACCX_Ale_dev, *ACCY_Ale_dev, *ACCZ_Ale_dev;

//extern double *X_dev, *Y_dev, *Z_dev;
extern double *X_cld_dev, *Y_cld_dev, *Z_cld_dev;
extern double *MNx_dev, *MNy_dev, *MNz_dev;

#endif /* __GLOBAL_VAR_LIST_H__ */

