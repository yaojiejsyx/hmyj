#ifndef __POISSON_SOLVER_H__
#define __POISSON_SOLVER_H__

//bicgstab.cpp
double bicgstab_solver
(
    int ns, double* xi, double* b, const unsigned int* coo_mark, 
    int nnz, const double* coo_value, const unsigned int* coo_row_idx, const unsigned int* coo_col_idx, 
    int maxit, double eps, int info
);

double bicgstab_solver2
(
    int ns, void (*matv)(int, double* xi, double* Axi),
    double* xi, double* b, int maxit, double eps, int info
);

//bicgstab_gpu.cu
double bicgstab_solver_GPU
(
    int n, int nnz, int* cooRowIndex, int* cooColIndex, double* cooVal,
    double* x_device, double* b_device,
    int maxit,double tol,int info
);

double bicgstab_solver2_GPU
(
    int n, void (*matv)(int, double* xi, double* Axi), 
    double* x_device, double* b_device, int maxit, double eps, int info,
    double*, double*, double*, double*
);

#endif /* __POISSON_SOLVER_H__ */

