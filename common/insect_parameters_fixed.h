#ifndef INSECT_PARA_H
#define INSECT_PARA_H

class Insect_Module
{
    public:
	double Re,Frequency,AirDensity,AirViscosity;
	double Dimensional_Mass,               Nondimensional_Mass;
	double Dimensional_WingLength,         Nondimensional_WingLength;
	double Dimensional_MeanWingChord,      Nondimensional_MeanWingChord;
	double Dimensional_WingArea,           Nondimensional_WingArea;
	double Dimensional_Gravity,            Nondimensional_Gravity;
	double Nondimensional_MOI[3][3];
	double Nondimensional_CoM[3];
	
	void Insect_Parameters_Initialize();
	void Insect_Parameters_Nondimensionalize();
	void Insect_Parameters_Update();
};

void Insect_Module :: Insect_Parameters_Initialize()
{
    Dimensional_Mass           =  0.2071*1e-3;
    Dimensional_WingLength     =  20.2*1e-3;
    Dimensional_MeanWingChord  =  0;
    Dimensional_WingArea       =  0;
    Dimensional_Gravity        =  9.81;
    
    Frequency                  =  65.0;
    
    /* temperature : 15 centigrade degrees  */
    AirDensity                 =  1.225;
    AirViscosity               =  1.78*1e-5;
}

void Insect_Module :: Insect_Parameters_Nondimensionalize()
{
    Re=pow(Dimensional_WingLength,2)*Frequency*AirDensity/AirViscosity;
    
    Nondimensional_Mass=Dimensional_Mass/(AirDensity*pow(Dimensional_WingLength,3))*1.0e15;
    Nondimensional_WingLength=1.0;
    Nondimensional_MeanWingChord=Dimensional_MeanWingChord/Dimensional_WingLength;
    Nondimensional_WingArea=Dimensional_WingArea/(pow(Dimensional_WingLength,2));
    Nondimensional_Gravity=Dimensional_Gravity/(Dimensional_WingLength*pow(Frequency,2))*0.0;

    Nondimensional_MOI[0][0]=9.9e15; Nondimensional_MOI[0][1]=0; Nondimensional_MOI[0][2]=0;
    Nondimensional_MOI[1][0]=0; Nondimensional_MOI[1][1]=9.9*1.0e15; Nondimensional_MOI[1][2]=-9.9*1.0e15;
    Nondimensional_MOI[2][0]=0; Nondimensional_MOI[2][1]=-9.9*1.0e15;Nondimensional_MOI[2][2]=9.9*1.0e15;

    Nondimensional_CoM[0]=0; Nondimensional_CoM[1]=-0.6930772277e-01; Nondimensional_CoM[2]=-2.168688267e-01;
}

void Insect_Module :: Insect_Parameters_Update()
{
    Re=pow(Dimensional_WingLength,2)*Frequency*AirDensity/AirViscosity;
    Nondimensional_Gravity=Dimensional_Gravity/(Dimensional_WingLength*pow(Frequency,2))*0.0;
}

#endif /* INSECT_PARA_H */
