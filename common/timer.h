#ifndef TIMER_H
#define TIMER_H

#include <stdio.h>
#include <time.h>

// A simple timer class

class Timer
{
  private:
    struct timespec start, stop;
    
  public:
    Timer() { }
    
    ~Timer() { }
    
    void Start()
    {
        clock_gettime(CLOCK_REALTIME, &start);
    }
    
    void Stop()
    {
        clock_gettime(CLOCK_REALTIME, &stop);
    }
    
    float Elapsed()
    {
        float elapsed;
        elapsed = (float)(stop.tv_sec - start.tv_sec) 
                + (float)(stop.tv_nsec - start.tv_nsec)*1e-9;
        return elapsed*1000; // ms
    }
};

#endif //TIMER_H
