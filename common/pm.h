#ifndef PM_H
#define PM_H

// #define USE_ART_DIFF
#define AD_ALPHA 5.0
#define AD_BETA  50.0

#define USE_LES
#define LES_Cs 0.6

#include "poisson_solver.h"

void computeSourceContributionOld();
void doProjectionMethod(int it, const int PM_Iter);
void calBoundaryUVW(void);
void reset(double*, int);
void residual(void);

void doProjectionMethod_GPU(int it, const int PM_Iter);

#endif /* PM_H */
