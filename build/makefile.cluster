HOME_DIR 			:= ..
BUILD_DIR 			:= $(HOME_DIR)/build

SRC_DIR				:= $(HOME_DIR)/src
SRC_FOLDERS			:= /
SRC_i_DIR			:= $(addprefix $(SRC_DIR), $(SRC_FOLDERS))
SRC					:= $(foreach sdir, $(SRC_i_DIR), $(wildcard $(sdir)/*.cpp)) \
                       $(foreach sdir, $(SRC_i_DIR), $(wildcard $(sdir)/*.cu))

INC_DIR			    := $(HOME_DIR)/common

OBJ_DIR				:= $(BUILD_DIR)/obj
OBJ_i_DIR			:= $(addprefix $(OBJ_DIR), $(SRC_FOLDERS))
OBJ    				:= $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(SRC))
OBJ					:= $(OBJ:.cpp=.o)
OBJ					:= $(OBJ:.cu=.o)
OBJ_DEP             := $(OBJ:.o=.o.d)

LIB_DIR 			:= $(BUILD_DIR)/lib
LIB 				:= $(LIB_DIR)/lapack_LINUX.a $(LIB_DIR)/blas_LINUX.a \
			   		   $(LIB_DIR)/tmglib_LINUX.a $(LIB_DIR)/libf2c.a \
			   		   $(LIB_DIR)/libtecio.a

TARGET_DIR 			:= $(HOME_DIR)/flapping
TARGET				:= $(TARGET_DIR)/flap

CXX					:= g++
FC					:= gfortran
CUDA_PATH           := "/app1/goldgpu/NVIDIA_GPU_Computing_SDK-5.5"
NVCC				:= $(CUDA_PATH)/bin/nvcc
FLAGS 				:= -Xcompiler -fopenmp -O3 -arch=sm_20
LINK 				:= -L$(CUDA_PATH)/lib64 -lcublas -lcusparse
INCLUDES            := -I${INC_DIR}

# dependency modifier
define dep-mod
@mv -f $(1).d $(1).tmp
@sed -e 's|.*:|'"$(1)"':|' < $(1).tmp > $(1).d
@sed -e 's/.*://' -e 's/\\$$//' < $(1).tmp | fmt -1 | \
  sed -e 's/^ *//' -e 's/$$/:/' >> $(1).d
@rm -f $(1).tmp
endef

.PHONY: clean checkdirs

$(TARGET) : checkdirs | $(OBJ)
	$(NVCC) $(FLAGS) $(OBJ) -o $(TARGET) $(LINK) $(LIB)
    
# pull in dependency info for *existing* .o files
-include $(OBJ_DEP)

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cu
	@echo $<
	@echo $@
	$(NVCC) $(FLAGS) -c $< -o $@ ${INCLUDES}
	@$(NVCC) -M $< -o $@.d ${INCLUDES}
	@$(call dep-mod, $@)

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cpp
	@echo $<
	@echo $@
	$(NVCC) $(FLAGS) -c $< -o $@ ${INCLUDES}
	@$(CXX) -MM $< -o $@.d ${INCLUDES}
	@$(call dep-mod, $@)

checkdirs : $(OBJ_i_DIR) $(TARGET_DIR)

$(OBJ_i_DIR) :
	@mkdir $@

$(TARGET_DIR) :
	@mkdir $(TARGET_DIR)

clean :
	rm -rf $(TARGET) $(OBJ_DIR)

